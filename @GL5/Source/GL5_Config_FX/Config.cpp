// ////////////////////////////////////////////////////////////////////////////
// GL5 v.1.0 Config FX
// 
// ////////////////////////////////////////////////////////////////////////////

class CfgPatches
{
	class GL5_Config_FX
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"A3_Data_F", "A3_Weapons_F", "A3_Data_F_ParticleEffects"};
	};
};

#include "CfgCloudlets.hpp"

#include "CfgLights.hpp"

#include "GL5_Config_FX\CfgAmmo.hpp"

#include "GL5_Config_FX\CfgHelicopter.hpp"

#include "GL5_Config_FX\CfgVehicle.hpp"
