// ////////////////////////////////////////////////////////////////////////////
// GL5 v.1.0 Sound FX Config
//
// ////////////////////////////////////////////////////////////////////////////

class CfgVehicles
{
	class Sound;

	class Sound_Fire : Sound
	{
		scope = 2;
		sound = "Fire";
		displayName = "$STR_DN_Sound_Fire";
	};

	class Fire_v01 : Sound
	{
		scope = 2;
		sound = "Fire_SFX_v01";
		displayName = "Fire v01";
	};

	class Fire_v02 : Sound
	{
		scope = 2;
		sound = "Fire_SFX_v02";
		displayName = "Fire v02";
	};

	class Fire_v03 : Sound
	{
		scope = 2;
		sound = "Fire_SFX_v03";
		displayName = "Fire v03";
	};

	class Fire_v04 : Sound
	{
		scope = 2;
		sound = "Fire_SFX_v04";
		displayName = "Fire v04";
	};

	class Fire_v05 : Sound
	{
		scope = 2;
		sound = "Fire_SFX_v05";
		displayName = "Fire v05";
	};

	class Fire_v06 : Sound
	{
		scope = 2;
		sound = "Fire_SFX_v06";
		displayName = "Fire v06";
	};

	class Fire_v07 : Sound
	{
		scope = 2;
		sound = "Fire_SFX_v07";
		displayName = "Fire v07";
	};

	class Flies_v01 : Sound
	{
		scope = 1;
		sound = "Flies_SFX_v01";
		displayName = "Flies";
	};

	class Lighthouse_v01 : Sound
	{
		scope = 1;
		sound = "Lighthouse_SFX_v01";
		displayName = "Lighthouse";
	};
};