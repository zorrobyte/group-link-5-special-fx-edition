// ////////////////////////////////////////////////////////////////////////////
// GL5 v.1.0 Sound FX Config
//
// ////////////////////////////////////////////////////////////////////////////

class CfgPatches 
{
	class GL5_Sound_FX
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 1.0;
		requiredAddons[] = {"A3_Data_F", "A3_Characters_F", "A3_Sounds_F"};
	};
};

#include "CfgSounds.hpp"

#include "CfgSFX.hpp"

#include "CfgVehicles.hpp"