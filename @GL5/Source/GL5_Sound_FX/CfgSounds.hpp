// ////////////////////////////////////////////////////////////////////////////
// GL5 v.1.0 Sound FX Config
//
// ////////////////////////////////////////////////////////////////////////////

class CfgSounds
{
	// //////////////////////////////////////
	// Bullet Sound FX
	// //////////////////////////////////////

	class Bullet_v01
	{
		name = "Bullet_v01";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Bullet_v01.ogg", db, 1};
		titles[] = {};
	};

	class Bullet_v02
	{
		name = "Bullet_v02";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Bullet_v02.ogg", db, 1};
		titles[] = {};
	};

	class Bullet_v03
	{
		name = "Bullet_v03";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Bullet_v03.ogg", db, 1};
		titles[] = {};
	};

	class Bullet_v04
	{
		name = "Bullet_v04";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Bullet_v04.ogg", db, 1};
		titles[] = {};
	};

	class Bullet_v05
	{
		name = "Bullet_v05";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Bullet_v05.ogg", db, 1};
		titles[] = {};
	};

	class Bullet_v06
	{
		name = "Bullet_v06";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Bullet_v06.ogg", db, 1};
		titles[] = {};
	};

	class Bullet_v07
	{
		name = "Bullet_v07";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Bullet_v07.ogg", db, 1};
		titles[] = {};
	};

	class Bullet_v08
	{
		name = "Bullet_v08";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Bullet_v08.ogg", db, 1};
		titles[] = {};
	};

	class Bullet_v09
	{
		name = "Bullet_v09";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Bullet_v09.ogg", db, 1};
		titles[] = {};
	};

	class Bullet_v10
	{
		name = "Bullet_v10";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Bullet_v10.ogg", db, 1};
		titles[] = {};
	};

	class Bullet_v11
	{
		name = "Bullet_v11";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Bullet_v11.ogg", db, 1};
		titles[] = {};
	};

	class Bullet_v12
	{
		name = "Bullet_v12";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Bullet_v12.ogg", db, 1};
		titles[] = {};
	};

	class Bullet_v13
	{
		name = "Bullet_v13";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Bullet_v13.ogg", db, 1};
		titles[] = {};
	};

	class Bullet_v14
	{
		name = "Bullet_v14";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Bullet_v14.ogg", db, 1};
		titles[] = {};
	};

	class Bullet_v15
	{
		name = "Bullet_v15";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Bullet_v15.ogg", db, 1};
		titles[] = {};
	};

	class Bullet_v16
	{
		name = "Bullet_v16";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Bullet_v16.ogg", db, 1};
		titles[] = {};
	};

	class Bullet_v17
	{
		name = "Bullet_v17";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Bullet_v17.ogg", db, 1};
		titles[] = {};
	};

	class Bullet_v18
	{
		name = "Bullet_v18";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Bullet_v18.ogg", db, 1};
		titles[] = {};
	};

	class Bullet_v19
	{
		name = "Bullet_v19";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Bullet_v19.ogg", db, 1};
		titles[] = {};
	};

	class Bullet_v20
	{
		name = "Bullet_v20";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Bullet_v20.ogg", db, 1};
		titles[] = {};
	};

	class Bullet_v21
	{
		name = "Bullet_v21";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Bullet_v21.ogg", db, 1};
		titles[] = {};
	};

	class Bullet_v22
	{
		name = "Bullet_v22";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Bullet_v22.ogg", db, 1};
		titles[] = {};
	};

	class Bullet_v23
	{
		name = "Bullet_v23";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Bullet_v23.ogg", db, 1};
		titles[] = {};
	};

	class Bullet_v24
	{
		name = "Bullet_v24";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Bullet_v24.ogg", db, 1};
		titles[] = {};
	};

	class Bullet_v25
	{
		name = "Bullet_v25";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Bullet_v25.ogg", db, 1};
		titles[] = {};
	};

	class Bullet_v26
	{
		name = "Bullet_v26";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Bullet_v26.ogg", db, 1};
		titles[] = {};
	};

	class Bullet_v27
	{
		name = "Bullet_v27";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Bullet_v27.ogg", db, 1};
		titles[] = {};
	};

	class Bullet_v28
	{
		name = "Bullet_v28";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Bullet_v28.ogg", db, 1};
		titles[] = {};
	};

	class Bullet_v29
	{
		name = "Bullet_v29";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Bullet_v29.ogg", db, 1};
		titles[] = {};
	};

	class Bullet_v30
	{
		name = "Bullet_v30";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Bullet_v30.ogg", db, 1};
		titles[] = {};
	};

	class Bullet_v31
	{
		name = "Bullet_v31";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Bullet_v31.ogg", db, 1};
		titles[] = {};
	};

	class Bullet_v32
	{
		name = "Bullet_v32";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Bullet_v32.ogg", db, 1};
		titles[] = {};
	};

	class Bullet_v33
	{
		name = "Bullet_v33";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Bullet_v33.ogg", db, 1};
		titles[] = {};
	};

	class Bullet_v34
	{
		name = "Bullet_v34";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Bullet_v34.ogg", db, 1};
		titles[] = {};
	};

	class Bullet_v35
	{
		name = "Bullet_v35";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Bullet_v35.ogg", db, 1};
		titles[] = {};
	};

	class Bullet_v36
	{
		name = "Bullet_v36";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Bullet_v36.ogg", db, 1};
		titles[] = {};
	};

	class Bullet_v37
	{
		name = "Bullet_v37";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Bullet_v37.ogg", db, 1};
		titles[] = {};
	};

	class Bullet_v38
	{
		name = "Bullet_v38";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Bullet_v22.ogg", db, 1};
		titles[] = {};
	};

	class Bullet_v39
	{
		name = "Bullet_v39";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Bullet_v24.ogg", db, 1};
		titles[] = {};
	};

	class Bullet_v40
	{
		name = "Bullet_v40";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Bullet_v25.ogg", db, 1};
		titles[] = {};
	};

	class Bullet_v41
	{
		name = "Bullet_v41";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Bullet_v26.ogg", db, 1};
		titles[] = {};
	};

	class Bullet_v42
	{
		name = "Bullet_v42";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Bullet_v27.ogg", db, 1};
		titles[] = {};
	};

	class Bullet_v43
	{
		name = "Bullet_v43";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Bullet_v28.ogg", db, 1};
		titles[] = {};
	};

	// //////////////////////////////////////
	// Shell Sound FX
	// //////////////////////////////////////

	class Shell_In_v01
	{
		name = "Shell_In_v01";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Shell_In_v01.ogg", db, 1};
		titles[] = {};
	};

	class Shell_In_v02
	{
		name = "Shell_In_v02";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Shell_In_v02.ogg", db, 1};
		titles[] = {};
	};

	class Shell_In_v03
	{
		name = "Shell_In_v03";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Shell_In_v03.ogg", db, 1};
		titles[] = {};
	};

	class Shell_In_v04
	{
		name = "Shell_In_v04";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Shell_In_v04.ogg", db, 1};
		titles[] = {};
	};

	class Shell_In_v05
	{
		name = "Shell_In_v05";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Shell_In_v05.ogg", db, 1};
		titles[] = {};
	};

	class Shell_In_v06
	{
		name = "Shell_In_v06";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Shell_In_v06.ogg", db, 1};
		titles[] = {};
	};

	class Shell_In_v07
	{
		name = "Shell_In_v07";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Shell_In_v07.ogg", db, 1};
		titles[] = {};
	};

	// //////////////////////////////////////
	// Sparks SFX
	// //////////////////////////////////////

	class Spark_v01
	{
		name = "Spark_v01";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Spark_v01.ogg", db, 1};
		titles[] = {};
	};

	class Spark_v02
	{
		name = "Spark_v02";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Spark_v02.ogg", db, 1};
		titles[] = {};
	};

	class Spark_v03
	{
		name = "Spark_v03";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Spark_v03.ogg", db, 1};
		titles[] = {};
	};

	class Spark_v04
	{
		name = "Spark_v04";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Spark_v04.ogg", db, 1};
		titles[] = {};
	};

	class Spark_v05
	{
		name = "Spark_v05";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Spark_v05.ogg", db, 1};
		titles[] = {};
	};

	class Spark_v06
	{
		name = "Spark_v06";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Spark_v06.ogg", db, 1};
		titles[] = {};
	};

	class Spark_v07
	{
		name = "Spark_v07";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Spark_v07.ogg", db, 1};
		titles[] = {};
	};

	class Spark_v08
	{
		name = "Spark_v08";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Spark_v08.ogg", db, 1};
		titles[] = {};
	};

	class Spark_v09
	{
		name = "Spark_v09";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Spark_v09.ogg", db, 1};
		titles[] = {};
	};

	// //////////////////////////////////////
	// Explosion Sound FX
	// //////////////////////////////////////

	class Explosion_v01
	{
		name = "Explosion_v01";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Explosion_v01.ogg", db, 1, 1500};
		titles[] = {};
	};

	class Explosion_v02
	{
		name = "Explosion_v02";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Explosion_v02.ogg", db, 1, 1500};
		titles[] = {};
	};

	class Explosion_v03
	{
		name = "Explosion_v03";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Explosion_v03.ogg", db, 1, 1500};
		titles[] = {};
	};

	class Explosion_v04
	{
		name = "Explosion_v04";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Explosion_v04.ogg", db, 1, 1500};
		titles[] = {};
	};

	class Explosion_v05
	{
		name = "Explosion_v05";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Explosion_v05.ogg", db, 1, 1500};
		titles[] = {};
	};

	class Explosion_v06
	{
		name = "Explosion_v06";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Explosion_v06.ogg", db, 1, 1500};
		titles[] = {};
	};

	class Explosion_v07
	{
		name = "Explosion_v07";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Explosion_v07.ogg", db, 1, 1500};
		titles[] = {};
	};

	class Explosion_v08
	{
		name = "Explosion_v08";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Explosion_v08.ogg", db, 1, 1500};
		titles[] = {};
	};

	class Explosion_v09
	{
		name = "Explosion_v09";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Explosion_v09.ogg", db, 1, 1500};
		titles[] = {};
	};

	class Explosion_v10
	{
		name = "Explosion_v10";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Explosion_v10.ogg", db, 1, 1500};
		titles[] = {};
	};

	class Explosion_v11
	{
		name = "Explosion_v11";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Explosion_v11.ogg", db, 1, 1500};
		titles[] = {};
	};

	class Explosion_v12
	{
		name = "Explosion_v12";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Explosion_v12.ogg", db, 1, 1500};
		titles[] = {};
	};

	class Explosion_v13
	{
		name = "Explosion_v13";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Explosion_v13.ogg", db, 1, 1500};
		titles[] = {};
	};

	class Explosion_v14
	{
		name = "Explosion_v14";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Explosion_v14.ogg", db, 1, 1500};
		titles[] = {};
	};

	class Explosion_v15
	{
		name = "Explosion_v15";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Explosion_v15.ogg", db, 1, 1500};
		titles[] = {};
	};

	class Explosion_v16
	{
		name = "Explosion_v16";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Explosion_v16.ogg", db, 1, 1500};
		titles[] = {};
	};

	class Explosion_v17
	{
		name = "Explosion_v17";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Explosion_v17.ogg", db, 1, 1500};
		titles[] = {};
	};

	class Explosion_v18
	{
		name = "Explosion_v18";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Explosion_v18.ogg", db, 1, 1500};
		titles[] = {};
	};

	class Explosion_v19
	{
		name = "Explosion_v19";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Explosion_v19.ogg", db, 1, 1500};
		titles[] = {};
	};

	class Explosion_v20
	{
		name = "Explosion_v20";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Explosion_v20.ogg", db, 1, 1500};
		titles[] = {};
	};

	class Explosion_v21
	{
		name = "Explosion_v21";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Explosion_v21.ogg", db, 1, 1500};
		titles[] = {};
	};

	class Explosion_v22
	{
		name = "Explosion_v22";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Explosion_v22.ogg", db, 1, 1500};
		titles[] = {};
	};

	class Explosion_v23
	{
		name = "Explosion_v23";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Explosion_v23.ogg", db, 1, 1500};
		titles[] = {};
	};

	class Explosion_v24
	{
		name = "Explosion_v24";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Explosion_v24.ogg", db, 1, 1500};
		titles[] = {};
	};

	class Explosion_v25
	{
		name = "Explosion_v25";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Explosion_v25.ogg", db, 1, 1500};
		titles[] = {};
	};

	class Explosion_v26
	{
		name = "Explosion_v26";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Explosion_v26.ogg", db, 1, 1500};
		titles[] = {};
	};

	class Explosion_v27
	{
		name = "Explosion_v27";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Explosion_v27.ogg", db, 1, 1500};
		titles[] = {};
	};

	class Explosion_v28
	{
		name = "Explosion_v28";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Explosion_v28.ogg", db, 1, 1500};
		titles[] = {};
	};

	// //////////////////////////////////////
	// Flies Sound FX
	// //////////////////////////////////////

	class Flies_v01
	{
		name = "Flies_v01";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Flies_v01.ogg", db-50, "0.85 + (random 0.5)", 50};
		titles[] = {};
	};

	class Flies_v02
	{
		name = "Flies_v02";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Flies_v02.ogg", db-50, "0.85 + (random 0.5)", 50};
		titles[] = {};
	};

	// //////////////////////////////////////
	// Church Sound FX
	// //////////////////////////////////////

	class Church_v01
	{
		name = "Church_v01";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Church_v01.ogg", db-15, 1, 500};
		titles[] = {};
	};

	class Church_v02
	{
		name = "Church_v02";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Church_v02.ogg", db-15, 1, 500};
		titles[] = {};
	};

	class Church_v03
	{
		name = "Church_v03";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Church_v03.ogg", db-15, 1, 500};
		titles[] = {};
	};

	class Church_v04
	{
		name = "Church_v04";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Church_v04.ogg", db-15, 1, 500};
		titles[] = {};
	};

	class Church_v05
	{
		name = "Church_v05";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Church_v05.ogg", db-15, 1, 500};
		titles[] = {};
	};

	class Church_v06
	{
		name = "Church_v06";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Church_v06.ogg", db-15, 1, 500};
		titles[] = {};
	};

	class Church_v07
	{
		name = "Church_v07";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Church_v07.ogg", db-15, 1, 500};
		titles[] = {};
	};

	// //////////////////////////////////////
	// Lighthouse Sound FX
	// //////////////////////////////////////

	class Lighthouse_v01
	{
		name = "Lighthouse_v01";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Lighthouse_v01.ogg", db, 1, 2500};
		titles[] = {};
	};

	class Lighthouse_v02
	{
		name = "Lighthouse_v02";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Lighthouse_v02.ogg", db, 1, 2500};
		titles[] = {};
	};

	class Lighthouse_v03
	{
		name = "Lighthouse_v03";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Lighthouse_v03.ogg", db, 1, 2500};
		titles[] = {};
	};

	class Lighthouse_v04
	{
		name = "Lighthouse_v04";
		sound[] = {"\GL5_Sound_FX\GL5_Sound\Lighthouse_v04.ogg", db, 1, 2500};
		titles[] = {};
	};
};