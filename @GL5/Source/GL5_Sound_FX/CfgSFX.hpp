// ////////////////////////////////////////////////////////////////////////////
// GL5 v.1.0 Sound SFX Config
//
// ////////////////////////////////////////////////////////////////////////////

class CfgSFX
{
	access = 1;

	class Fire
	{
		sounds[] = {"Fire1"};
		name = $STR_CFG_SFX_FIRE;
		Fire1[] = {"\GL5_Sound_FX\GL5_Sound\Fire_v01.ogg", 1, 1, 50, 1, 0, 0, 0};
		empty[] = {"", 0, 0, 0, 0, 1, 5, 20};
	};

	class Fire_SFX_v01
	{
		sounds[] = {"Fire1"};
		name = "Fire v01";
		Fire1[] = {"\GL5_Sound_FX\GL5_Sound\Fire_v01.ogg", 1, 1, 200, 1, 0, 0, 0};
		empty[] = {"", 0, 0, 0, 0, 1, 5, 20};
	};

	class Fire_SFX_v02
	{
		sounds[] = {"Fire1"};
		name = "Fire v02";
		Fire1[] = {"\GL5_Sound_FX\GL5_Sound\Fire_v02.ogg", 1, 1, 200, 1, 0, 0, 0};
		empty[] = {"", 0, 0, 0, 0, 1, 5, 20};
	};

	class Fire_SFX_v03
	{
		sounds[] = {"Fire1"};
		name = "Fire v03";
		Fire1[] = {"\GL5_Sound_FX\GL5_Sound\Fire_v03.ogg", 1, 1, 200, 1, 0, 0, 0};
		empty[] = {"", 0, 0, 0, 0, 1, 5, 20};
	};

	class Fire_SFX_v04
	{
		sounds[] = {"Fire1"};
		name = "Fire v04";
		Fire1[] = {"\GL5_Sound_FX\GL5_Sound\Fire_v04.ogg", 1, 1, 200, 1, 0, 0, 0};
		empty[] = {"", 0, 0, 0, 0, 1, 5, 20};
	};

	class Fire_SFX_v05
	{
		sounds[] = {"Fire1"};
		name = "Fire v05";
		Fire1[] = {"\GL5_Sound_FX\GL5_Sound\Fire_v05.ogg", 1, 1, 200, 1, 0, 0, 0};
		empty[] = {"", 0, 0, 0, 0, 1, 5, 20};
	};

	class Fire_SFX_v06
	{
		sounds[] = {"Fire1"};
		name = "Fire v06";
		Fire1[] = {"\GL5_Sound_FX\GL5_Sound\Fire_v06.ogg", 1, 1, 200, 1, 0, 0, 0};
		empty[] = {"", 0, 0, 0, 0, 1, 5, 20};
	};

	class Fire_SFX_v07
	{
		sounds[] = {"Fire1"};
		name = "Fire v07";
		Fire1[] = {"\GL5_Sound_FX\GL5_Sound\Fire_v07.ogg", 1, 1, 200, 1, 0, 0, 0};
		empty[] = {"", 0, 0, 0, 0, 1, 5, 20};
	};

	class Flies_SFX_v01
	{
		sounds[] = {"Flies1", "Flies2"};
		name = "Flies";
		Flies1[] = {"\GL5_Sound_FX\GL5_Sound\Flies_v01.ogg", 0.0577828, 1, 50, 0.5, 0, 0, 0};
		Flies2[] = {"\GL5_Sound_FX\GL5_Sound\Flies_v02.ogg", 0.0577828, 1, 50, 0.5, 0, 0, 0};
		empty[] = {"", 0, 0, 0, 0, 0, 0, 0};
	};

	class Lighthouse_SFX_v01
	{
		sounds[] = {"Lighthouse1", "Lighthouse2"};
		name = "Lighthouse";
		Lighthouse1[] = {"\GL5_Sound_FX\GL5_Sound\Lighthouse_v01.ogg", 10, 1, 1500, 0.5, 50, 50, 50};
		Lighthouse2[] = {"\GL5_Sound_FX\GL5_Sound\Lighthouse_v02.ogg", 10, 1, 1500, 0.5, 50, 50, 50};
		empty[] = {"", 0, 0, 0, 0, 0, 0, 0};
	};
};
