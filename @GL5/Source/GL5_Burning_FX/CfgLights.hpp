class CfgLights
{
	class SmallFireLight
	{
		color[] = {0, 0, 0};
		ambient[] = {0, 0, 0};
		brightness = 0;
		diffuse[] = {0.5, 0.5, 0.5};
		position[] = {0, 0, 0};
	};
};