// ////////////////////////////////////////////////////////////////////////////
// GL5 v.1.0 Burning FX Config
//
// ////////////////////////////////////////////////////////////////////////////

class CfgPatches
{
	class GL5_Burning_FX
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 1.0;
		requiredAddons[] = {"A3_Data_F", "A3_Misc_F"};
	};
};

#include "CfgResource.hpp"

#include "CfgLights.hpp"

class Extended_Init_Eventhandlers
{
	class Land_Fire_burning
	{
		class GL5_Land_Fire_burning
		{
			init = " (_this select 0) inflame True; _this execVM ""\GL5_Burning_FX\GL5_Camp_FX\GL5_Camp_FX.sqf"" ";
		};
	};
};

class CfgVehicles
{
	class HouseBase;

	class House : HouseBase
	{
		class EventHandlers
		{
			hit = " _this call (GL5_EH_Hit_F select 0) ";
		};
	};

	class Land_Fire : House
	{
		class EventHandlers
		{
			init = " _this execVM ""\GL5_Burning_FX\GL5_Camp_FX\GL5_Camp_FX.sqf"" ";
		};
	};

	class Land_Campfire_f : Land_Fire
	{
		class EventHandlers
		{
			init = " (_this select 0) inflame True; _this execVM ""\GL5_Burning_FX\GL5_Camp_FX\GL5_Camp_FX.sqf"" ";
		};
	};
};