// ////////////////////////////////////////////////////////////////////////////
// GL5 v.1.0 Resource Config
// ////////////////////////////////////////////////////////////////////////////

class RscText;

class RscTitles
{
	class GL5_Burning
	{
		idd = 0;
		movingEnable = 0;
		duration = 1;

		name = "GL5_Burning";

		controls[] = {"GL5_Burning"};

		class GL5_Burning : RscText
		{
 			idc = -1;
			type = 0;
			style = 0x02;

			colorText[] = {0.7, 0, 0, 1};

			colorBackground[] = { 0, 0, 0, 0};

			font = "PuristaMedium";

			size = 5;

			sizeEx = 0.035; 

			x = 0.02;
			y = 0.0;
			w = 0.1;
			h = 0.1;

			text = "Burning!";
		};
	};
};