// ////////////////////////////////////////////////////////////////////////////
// GL5 v.1.0 Blood FX Config
// Script and Models By =\SNKMAN/=
// Images By Sgt.Ace
// ////////////////////////////////////////////////////////////////////////////

class CfgPatches
{
	class GL5_Blood_FX
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 1.04;
		requiredAddons[] = {"A3_Data_F"};
	};
};
//fix by Lordprimate
#include "CfgResource.hpp"

#define GL5_RscBlood "GL5_RscBleeding_Img_01 = ""\GL5_Blood_FX\GL5_Images\GL5_Bleeding_01_ca.paa""; GL5_RscBleeding_Img_02 = ""\GL5_Blood_FX\GL5_Images\GL5_Bleeding_02_ca.paa""; ";

class CfgVehicles
{
	class NonStrategic;

	class GL5_Blood_FX : NonStrategic
	{
		scope = 2;
	};

	class GL5_Blood_01 : GL5_Blood_FX
	{
		model = "\GL5_Blood_FX\GL5_Models\GL5_Blood_01.p3d";

		class Eventhandlers
		{
			Init = GL5_RscBlood
		};
	};

	class GL5_Blood_02 : GL5_Blood_FX
	{
		model = "\GL5_Blood_FX\GL5_Models\GL5_Blood_02.p3d";
	};

	class GL5_Blood_03 : GL5_Blood_FX
	{
		model = "\GL5_Blood_FX\GL5_Models\GL5_Blood_03.p3d";
	};

	class GL5_Blood_04 : GL5_Blood_FX
	{
		model = "\GL5_Blood_FX\GL5_Models\GL5_Blood_04.p3d";
	};
};

class CfgAddOns
{
	class PreloadBanks {};
	
	class PreloadAddOns
	{
		class GL5_Blood_FX
		{
			list[] = {"GL5_Blood_FX"};
		};
	};
};