// ////////////////////////////////////////////////////////////////////////////
// GL5 v.1.0 Resource Config
// ////////////////////////////////////////////////////////////////////////////
//Fixes by Lordprimate
class RscText;

class RscTitles
{
	class GL5_RscBleeding_01
	{
		idd = 0;
		movingEnable = 0;
		duration = 1;

		name = "GL5_RscBleeding_01";

		controls[] = {"GL5_RscBleeding_01"};

		class GL5_RscBleeding_01 : RscText
		{
 			idc = -1;
			type = 0;
			style = 0x02;

			colorText[] = {0.7, 0, 0, 1};

			colorBackground[] = { 0, 0, 0, 0};

			font = "PuristaMedium";

			size = 5;

			sizeEx = 0.035; 

			x = 0.02;
			y = 0.0;
			w = 0.1;
			h = 0.1;

			text = "Bleeding!";
		};
	};

 	class GL5_RscBleeding_02
 	{ 
  		idd = -1;
		onLoad = "(_this select 0) displayCtrl 23850 ctrlSetText GL5_RscBleeding_Img_01";

  		movingEnable = False;

  		duration = "(getDammage player * 15) + 1";

  		fadein = 0.1;

  		name = "GL5_RscBleeding_02";
  		controls[]= {GL5_RscBleeding_02};

  		class GL5_RscBleeding_02
		{
  			idc = 23850;
   			type = 0;
  			style = 48;

  			colorText[]= {1, 1, 1, 0.8};
  			colorBackground[]= {0, 0, 0, 0};

  			font = "PuristaMedium";

  			sizeEx =1 ;

  			x = "SafeZoneXAbs";
  			y = "SafeZoneY";
  			w = "SafeZoneWAbs";
  			h = "SafeZoneH";

  			text = "";
		};
	};

 	class GL5_RscBleeding_03
 	{ 
  		idd = -1;
  		onLoad = "(_this select 0) displayCtrl 23850 ctrlSetText GL5_RscBleeding_Img_02";

  		movingEnable = False;

  		duration = "(getDammage player * 15) + 1";

  		fadein = 0.1;

  		name = "GL5_RscBleeding_03";
  		controls[]= {GL5_RscBleeding_03};

  		class GL5_RscBleeding_03
		{
  			idc = 23850;
   			type = 0;
  			style = 48;

  			colorText[]= {1, 1, 1, 0.8};
  			colorBackground[]= {0, 0, 0, 0};

  			font = "PuristaMedium";

  			sizeEx =1 ;

  			x = "SafeZoneXAbs";
  			y = "SafeZoneY";
  			w = "SafeZoneWAbs";
  			h = "SafeZoneH";

  			text = "";
		};
	};
};