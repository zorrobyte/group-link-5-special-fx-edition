// ////////////////////////////////////////////////////////////////////////////
// GL5 v.1.0
// ////////////////////////////////////////////////////////////////////////////
// Initialize Functions
// Script by =\SNKMAN/=
// ////////////////////////////////////////////////////////////////////////////
private ["_a","_b"];

_a = _this select 0;

if (isServer) then
{
	[_a] spawn
	{
		waitUntil { ( !(isNil "GL5_Defend") || (time > 1) ) };

		if (isNil "GL5_Defend") then
		{
			if (isNil "dg1") then
			{
				GL5_Defend = [ [] ];

				GL5_Synchronize set [0, True];
			}
			else
			{
				call compile preprocessFile (GL5_Path+"GL5\GL5_System\GL5_Defend.sqf");
			};
		};
	};

	[_a] spawn
	{
		waitUntil { ( !(isNil "GL5_Static") || (time > 1) ) };

		if (isNil "GL5_Static") then
		{
			if (isNil "sg1") then
			{
				GL5_Static = [ [] ];

				GL5_Synchronize set [1, True];
			}
			else
			{
				call compile preprocessFile (GL5_Path+"GL5\GL5_System\GL5_Static.sqf");
			};
		};
	};

	[_a] spawn
	{
		waitUntil { ( !(isNil "GL5_Reinforcement") || (time > 1) ) };

		if (isNil "GL5_Reinforcement") then
		{
			if (isNil "cg1") then
			{
				GL5_Reinforcement = [ [], [], [], True ];

				GL5_Synchronize set [2, True];
			}
			else
			{
				call compile preprocessFile (GL5_Path+"GL5\GL5_System\GL5_Custom.sqf");
			};
		};
	};

	GL5_Groups = [ [] ];

	GL5_System = [ False, time, objNull, objNull, "Logic" createVehicleLocal [0,0,0], "Logic" createVehicleLocal [0,0,0], time + 50, [], [], [], True, [] ];

	GL5_Artillery = [ [], True ];

	GL5_Airstrike = [ [], True ];

	GL5_House_Search = [ [], [] ];

	GL5_Advancing = [ [], True, 0 ];

	GL5_Suppressed = [ [], time, 0, time ];

	GL5_Body_Detect = [ [], [] ];

	GL5_Detection = [ [] ];

	GL5_Extraction = [ [], [], [], "Logic" createVehicleLocal [0,0,0] ];

	GL5_Killed = [ [] ];

	GL5_Idle = [ [], [], [], [] ];

	GL5_Garrison = [ [] ];

	GL5_Static_Weapon = [ [], [] ];

	GL5_Car_Weapon = [ [], [] ];

	GL5_Patrol = [ [] ];

	waitUntil { !(isNil "GL5_Reinforcement") };

	call compile preprocessFile (GL5_Path+"GL5\GL5_System\GL5_Initialize.sqf");

	if (GL5_Reinforcement select 3) then
	{
 		GL5_Synchronize set [0, True];

		GL5_Synchronize set [1, True];

		GL5_Synchronize set [2, True];
	};

	call compile preprocessFile (GL5_Path+"GL5\GL5_System\GL5_Groups.sqf");

	call compile preprocessFile (GL5_Path+"GL5\GL5_Functions\GL5_System_F.sqf");

	call compile preprocessFile (GL5_Path+"GL5\GL5_Functions\GL5_System_F\GL5_Debug_F.sqf");

	call compile preprocessFile (GL5_Path+"GL5\GL5_Functions\GL5_System_F\GL5_Marker_F.sqf");

	call compile preprocessFile (GL5_Path+"GL5\GL5_Functions\GL5_System_F\GL5_Weapon_F.sqf");

	call compile preprocessFile (GL5_Path+"GL5\GL5_Functions\GL5_Feature_F\GL5_Advancing_F.sqf");

	call compile preprocessFile (GL5_Path+"GL5\GL5_Functions\GL5_Feature_F\GL5_Airstrike_F.sqf");

	call compile preprocessFile (GL5_Path+"GL5\GL5_Functions\GL5_Feature_F\GL5_Artillery_F.sqf");

	call compile preprocessFile (GL5_Path+"GL5\GL5_Functions\GL5_Feature_F\GL5_Body_Detect_F.sqf");

	call compile preprocessFile (GL5_Path+"GL5\GL5_Functions\GL5_Feature_F\GL5_Body_Remove_F.sqf");

	call compile preprocessFile (GL5_Path+"GL5\GL5_Functions\GL5_Feature_F\GL5_Vehicle_Mount_F.sqf");

	call compile preprocessFile (GL5_Path+"GL5\GL5_Functions\GL5_Feature_F\GL5_Car_Weapon_F.sqf");

	call compile preprocessFile (GL5_Path+"GL5\GL5_Functions\GL5_Feature_F\GL5_Detection_F.sqf");

	call compile preprocessFile (GL5_Path+"GL5\GL5_Functions\GL5_Feature_F\GL5_Extraction_F.sqf");

	call compile preprocessFile (GL5_Path+"GL5\GL5_Functions\GL5_Feature_F\GL5_Garrison_F.sqf");

	call compile preprocessFile (GL5_Path+"GL5\GL5_Functions\GL5_Feature_F\GL5_Helicopter_F.sqf");

	call compile preprocessFile (GL5_Path+"GL5\GL5_Functions\GL5_Feature_F\GL5_House_Search_F.sqf");

	call compile preprocessFile (GL5_Path+"GL5\GL5_Functions\GL5_Feature_F\GL5_Idle_F.sqf");

	call compile preprocessFile (GL5_Path+"GL5\GL5_Functions\GL5_Feature_F\GL5_Patrol_F.sqf");

	call compile preprocessFile (GL5_Path+"GL5\GL5_Functions\GL5_Feature_F\GL5_Reinforcement_F.sqf");

	call compile preprocessFile (GL5_Path+"GL5\GL5_Functions\GL5_Feature_F\GL5_Remount_F.sqf");

	call compile preprocessFile (GL5_Path+"GL5\GL5_Functions\GL5_Feature_F\GL5_Static_Weapon_F.sqf");

	call compile preprocessFile (GL5_Path+"GL5\GL5_Functions\GL5_Feature_F\GL5_Suppressed_F.sqf");

	call compile preprocessFile (GL5_Path+"GL5\GL5_Functions\GL5_Feature_F\GL5_Unmount_F.sqf");

	waitUntil { ( (GL5_Synchronize select 0) && (GL5_Synchronize select 1) && (GL5_Synchronize select 2) ) };

	if (count (GL5_Defend select 0) > 0) then
	{
		_b = (GL5_Defend select 0);

		GL5_Static set [0, (GL5_Static select 0) + _b];
	};

	[] execVM (GL5_Path+"GL5\GL5_System\GL5_Time.sqf");

	[3] execVM (GL5_Path+"GL5\GL5_System\GL5_Monitor.sqf");

	[] execVM (GL5_Path+"GL5\GL5_Features\GL5_Body_Detect\GL5_Body_Detect.sqf");

	[] execVM (GL5_Path+"GL5\GL5_Features\GL5_Body_Remove\GL5_Body_Remove.sqf");

	[] execVM (GL5_Path+"GL5\GL5_Features\GL5_Garrison\GL5_Garrison.sqf");

	[] execVM (GL5_Path+"GL5\GL5_Features\GL5_Idle\GL5_Idle.sqf");

	[] execVM (GL5_Path+"GL5\GL5_Features\GL5_Patrol\GL5_Patrol.sqf");

	[] execVM (GL5_Path+"GL5\GL5_Features\GL5_Static_Weapon\GL5_Static_Weapon.sqf");

	call compile preprocessFile (GL5_Path+"GL5\GL5_System\GL5_Twice.sqf");

	[] spawn (GL5_Debug_F select 0);

	[] spawn (GL5_Marker_F select 0);

	[] execVM (GL5_Path+"GL5\GroupLink5.sqf");
};