// ////////////////////////////////////////////////////////////////////////////
// GL5 v.1.0
// ////////////////////////////////////////////////////////////////////////////
// Local Database
// Based on Operation Flashpoint Mod E.C.P. ( Enhanced Configuration Project )
// 
// ////////////////////////////////////////////////////////////////////////////

GL5_Local = [

	[True, True],

	False,
	200,
	[ True, True, 50 ],

	True,
	True,
	500,
	100,

	70,
	500,

	35,
	2,
	1.5,
	1,
	10,
	-2,
	2,

	35,
	6,

	0.5,
	1,
	[ [0.1,0,0,1], [0.1,0,0,1], [0.1,0,0,1], [0.1,0,0,1] ],

	// #22
	True,

	// #23
	True,
	True,
	50,
	True,
	100,
	0,
	5,

	// #30
	True,
	1,
	100,
	True,
	100,
	True,

	// #36
	True,
	1,
	100,
	100,
	100,
	100,
	True,

	// #43
	True,
	1,
	100,
	100,

	// #47
	True,
	viewDistance,
	1,

	100,
	[0,1,2,3,4,5,6,7],

	100,
	[8,9,10,11,12,13,14],

	100,
	[8,9,10,11,12,13,14,15,16,17,18,19,20,21],

	True,
	100,
	100,
	100,
	100,
	100,
	100,
	100,
	True,
	100,

	// #66
	True,
	viewDistance,
	100,
	100,
	100,

	// #71
	True,
	True,
	True,
	True,

	// #75
	True,
	100,

	// #77
	True,
	True,
	True,

	// #80
	True,
	True,

	// #82
	True,
	300,

	// #84
	True,
	50,

	// #86
	True,

	// #87
	False
];

if (GL5_Path == "\GL5_System\") then
{
	call compile preprocessFile "\UserConfig\GL5\GL5_Local.sqf";
};