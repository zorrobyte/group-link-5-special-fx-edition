// ////////////////////////////////////////////////////////////////////////////
// GL5 v.1.0
// ////////////////////////////////////////////////////////////////////////////
// Twice
// Script by =\SNKMAN/=
// ////////////////////////////////////////////////////////////////////////////
private ["_a","_b","_c","_d"];

_a = (GL5_Reinforcement select 0);

if (count _a > 0) then
{
	_b = [];

	_c = 0;

	while { (_c < count _a) } do
	{
		_d = (_a select _c);

		if (_d in _b) then
		{
			GL5_Reinforcement set [0, (GL5_Reinforcement select 0) - [_d] ];

			GL5_Reinforcement set [0, (GL5_Reinforcement select 0) + [_d] ];
		}
		else
		{
			_b = _b + [_d];
		};

		_c = _c + 1;
	};
};

_a = (GL5_Static select 0);

if (count _a > 0) then
{
	_b = [];

	_c = 0;

	while { (_c < count _a) } do
	{
		_d = (_a select _c);

		if (_d in _b) then
		{
			GL5_Static set [0, (GL5_Static select 0) - [_d] ];

			GL5_Static set [0, (GL5_Static select 0) + [_d] ];
		}
		else
		{
			_b = _b + [_d];
		};

		_c = _c + 1;
	};
};

_a = (GL5_Location select 0);

if (count _a > 0) then
{
	_b = [];

	_c = 0;

	while { (_c < count _a) } do
	{
		_d = (_a select _c);

		if (_d in _b) then
		{
			GL5_Location set [0, (GL5_Location select 0) - [_d] ];

			GL5_Location set [0, (GL5_Location select 0) + [_d] ];
		}
		else
		{
			_b = _b + [_d];
		};

		_c = _c + 1;
	};
};