// ////////////////////////////////////////////////////////////////////////////
// GL5 v.1.0
// ////////////////////////////////////////////////////////////////////////////
// Special FX
// Script by =\SNKMAN/=
// ////////////////////////////////////////////////////////////////////////////
private ["_a","_b","_c","_d","_e"];

_a = _this select 0;

_b = time + 1;

waitUntil { (time > _b) };

["Special FX", 1] execVM (GL5_Path+"GL5\GL5_System\GL5_Display.sqf");

if (_a isKindOf "Logic") then
{
	if (isNil "GL5_Initialize") then
	{
		hint "Error: The ""Group Link 5: System"" module is required to initialize the ""Group Link 5: Special FX"" module.";
	}
	else
	{
		if (isServer) then
		{
			GL5_Dubbing = [ [] ];

			_c = allGroups;

			_d = 0;

			while { (_d < count _c) } do
			{
				_e = (_c select _d);

				GL5_Dubbing set [0, (GL5_Dubbing select 0) + [_e] ];

				_d = _d + 1;
			};

			if (count (GL5_Dubbing select 0) > 0) then
			{
				[] execVM (GL5_Path+"GL5\GL5_AddOns\GL5_Dubbing\GL5_Dubbing.sqf");
			};
		}
		else
		{
			GL5_AddOn_PublicVariable_CompileExecVM = compile preprocessFile (GL5_Path+"GL5\GL5_Addons\GL5_AddOn_PublicVariable.sqf");

			"GL5_AddOn_PublicVariable" addPublicVariableEventHandler { (_this select 1) call GL5_AddOn_PublicVariable_CompileExecVM };
		};

		call compile preprocessFile (GL5_Path+"GL5\GL5_System\GL5_AddOns.sqf");
	};
}
else
{
	if (isServer) then
	{
		GL5_Dubbing = [ [] ];

		_c = allGroups;

		_d = 0;

		while { (_d < count _c) } do
		{
			_e = (_c select _d);

			GL5_Dubbing set [0, (GL5_Dubbing select 0) + [_e] ];

			_d = _d + 1;
		};

		if (count (GL5_Dubbing select 0) > 0) then
		{
			[] execVM (GL5_Path+"GL5\GL5_AddOns\GL5_Dubbing\GL5_Dubbing.sqf");
		};
	}
	else
	{
		GL5_AddOn_PublicVariable_CompileExecVM = compile preprocessFile (GL5_Path+"GL5\GL5_Addons\GL5_AddOn_PublicVariable.sqf");

		"GL5_AddOn_PublicVariable" addPublicVariableEventHandler { (_this select 1) call GL5_AddOn_PublicVariable_CompileExecVM };
	};

	call compile preprocessFile (GL5_Path+"GL5\GL5_System\GL5_AddOns.sqf");
};

["Special FX", 2] execVM (GL5_Path+"GL5\GL5_System\GL5_Display.sqf");