// ////////////////////////////////////////////////////////////////////////////
// GL5 v.1.0
// ////////////////////////////////////////////////////////////////////////////
// Initialize
// Script by =\SNKMAN/=
// ////////////////////////////////////////////////////////////////////////////
private ["_a","_b","_c","_d","_e"];

_a = [];

_b = allGroups;

_c = 0;

while { (_c < count _b) } do
{
	_d = (_b select _c);

	_e = (vehicle leader _d);

	if (captive _e) then
	{
		_e setCaptive False;

		_a = _a + [_e];
	};

	if (side _e in _this) then
	{
		GL5_Groups set [0, (GL5_Groups select 0) + [_d] ];

		if (GL5_Reinforcement select 3) then
		{
			GL5_Reinforcement set [0, (GL5_Reinforcement select 0) + [_d] ];
		};

		if (_e isKindOf "StaticMortar") then
		{
			if (canFire _e) then
			{
				GL5_Artillery set [0, (GL5_Artillery select 0) + [_d] ];
			};
		};

		if (_e isKindOf "Plane") then
		{
			if ( (canFire _e) && (count waypoints _e == 1) ) then
			{
				_e setFuel 0;

				GL5_Airstrike set [0, (GL5_Airstrike select 0) + [_d] ];
			};
		};
	};

	_c = _c + 1;
};

if (count _a > 0) then
{
	{_x setCaptive True} forEach _a;
};