// ////////////////////////////////////////////////////////////////////////////
// GL5 v.1.0
// ////////////////////////////////////////////////////////////////////////////
// AddOns
// Script by =\SNKMAN/=
// ////////////////////////////////////////////////////////////////////////////
private ["_a","_b","_c","_d"];

if (isDedicated) then
{
	_a = False;

	GL5_Local set [4, False];
	GL5_Local set [5, False];
	GL5_Local set [22, False];
	GL5_Local set [23, False];
	GL5_Local set [30, False];
	GL5_Local set [36, False];
	GL5_Local set [43, False];
	GL5_Local set [47, False];
	GL5_Local set [66, False];
	GL5_Local set [71, False];
	GL5_Local set [80, False];
	GL5_Local set [82, False];
}
else
{
	_a = True;
};

_b = 15;

_c = 0;

while { (_c < _b) } do
{
	switch (_c) do
	{
		case 0 :
		{
			if (GL5_Local select 1) then
			{
				_d = isClass (configFile >> "cfgPatches" >> "GL5_Dubbing");

				if (_d) then
				{
					//	call compile preprocessFile (GL5_Path+"GL5\GL5_AddOns\GL5_Functions\GL5_Dubbing_F.sqf");

					if (_a) then
					{
						[_c] call compile preprocessFile (GL5_Path+"GL5\GL5_AddOns\GL5_Database\GL5_Initialize.sqf");

						call compile preprocessFile (GL5_Path+"GL5\GL5_AddOns\GL5_Functions\GL5_Grenade_F.sqf");

						call compile preprocessFile (GL5_Path+"GL5\GL5_AddOns\GL5_Functions\GL5_Incoming_F.sqf");

						call compile preprocessFile (GL5_Path+"GL5\GL5_AddOns\GL5_Functions\GL5_Reloading_F.sqf");

						[] execVM (GL5_Path+"GL5\GL5_AddOns\GL5_Reloading\GL5_Reloading.sqf");
					};
				}
				else
				{
					GL5_Local set [1, False];
				};
			};
		};

		case 1 :
		{
			if (GL5_Local select 4) then
			{
				_d = isClass (configFile >> "cfgPatches" >> "GL5_Blood_FX");

				if (_d) then
				{
					[_c] call compile preprocessFile (GL5_Path+"GL5\GL5_AddOns\GL5_Database\GL5_Initialize.sqf");

					if (GL5_Local select 5) then
					{
						call compile preprocessFile (GL5_Path+"GL5\GL5_AddOns\GL5_Functions\GL5_Blood_F\GL5_Bleeding_F.sqf");
					}
					else
					{
						GL5_Local set [5, False];
					};

					call compile preprocessFile (GL5_Path+"GL5\GL5_AddOns\GL5_Functions\GL5_Blood_F\GL5_Monitor_F.sqf");

					call compile preprocessFile (GL5_Path+"GL5\GL5_AddOns\GL5_Functions\GL5_Blood_F\GL5_Blood_FX_F.sqf");
				}
				else
				{
					GL5_Local set [4, False];

					GL5_Local set [5, False];
				};
			};
		};

		case 2 :
		{
			if (GL5_Local select 22) then
			{
				_d = isClass (configFile >> "cfgPatches" >> "GL5_Radio_Chatter");

				if (_d) then
				{
					call compile preprocessFile (GL5_Path+"GL5\GL5_AddOns\GL5_Functions\GL5_Chatter_F.sqf");

					[] execVM (GL5_Path+"GL5\GL5_AddOns\GL5_Chatter\GL5_Chatter.sqf");
				}
				else
				{
					GL5_Local set [22, False];
				};
			};
		};

		case 3 :
		{
			if (GL5_Local select 23) then
			{
				_d = isClass (configFile >> "cfgPatches" >> "GL5_Sound_FX");

				if (_d) then
				{
					[_c] call compile preprocessFile (GL5_Path+"GL5\GL5_AddOns\GL5_Database\GL5_Initialize.sqf");

					call compile preprocessFile (GL5_Path+"GL5\GL5_AddOns\GL5_Functions\GL5_Sound_FX_F.sqf");
				}
				else
				{
					GL5_Local set [23, False];
				};
			};
		};

		case 4 :
		{
			if (GL5_Local select 30) then
			{
				[_c] call compile preprocessFile (GL5_Path+"GL5\GL5_AddOns\GL5_Database\GL5_Initialize.sqf");

				call compile preprocessFile (GL5_Path+"GL5\GL5_AddOns\GL5_Functions\GL5_Weapon_FX_F.sqf");
			};
		};

		case 5 :
		{
			if (GL5_Local select 36) then
			{
				[_c] call compile preprocessFile (GL5_Path+"GL5\GL5_AddOns\GL5_Database\GL5_Initialize.sqf");

				call compile preprocessFile (GL5_Path+"GL5\GL5_AddOns\GL5_Functions\GL5_Shell_FX_F.sqf");
			};
		};

		case 6 :
		{
			if (GL5_Local select 43) then
			{
				call compile preprocessFile (GL5_Path+"GL5\GL5_AddOns\GL5_Functions\GL5_Impact_FX_F.sqf");
			};
		};

		case 7 :
		{
			if (GL5_Local select 47) then
			{
				[_c] call compile preprocessFile (GL5_Path+"GL5\GL5_AddOns\GL5_Database\GL5_Initialize.sqf");

				call compile preprocessFile (GL5_Path+"GL5\GL5_AddOns\GL5_Functions\GL5_Explosion_FX_F.sqf");
			};
		};

		case 8 :
		{
			if (GL5_Local select 66) then
			{
				[_c] call compile preprocessFile (GL5_Path+"GL5\GL5_AddOns\GL5_Database\GL5_Initialize.sqf");

				call compile preprocessFile (GL5_Path+"GL5\GL5_AddOns\GL5_Functions\GL5_Wrack_FX_F.sqf");
			};
		};

		case 9 :
		{
			if (GL5_Local select 71) then
			{
				_d = isClass (configFile >> "cfgPatches" >> "GL5_Burning_FX");

				if (_d) then
				{
					[_c] call compile preprocessFile (GL5_Path+"GL5\GL5_AddOns\GL5_Database\GL5_Initialize.sqf");

					call compile preprocessFile (GL5_Path+"GL5\GL5_AddOns\GL5_Functions\GL5_Burning_FX_F.sqf");

					call compile preprocessFile (GL5_Path+"GL5\GL5_AddOns\GL5_Functions\GL5_Camp_FX_F.sqf");

					call compile preprocessFile (GL5_Path+"GL5\GL5_AddOns\GL5_Functions\GL5_House_FX_F.sqf");
				}
				else
				{
					GL5_Local set [71, False];
				};
			};
		};

		case 10 :
		{
			if (GL5_Local select 75) then
			{
				if (_a) then
				{
					[_c] call compile preprocessFile (GL5_Path+"GL5\GL5_AddOns\GL5_Database\GL5_Initialize.sqf");
				};

				if (isServer) then
				{
					call compile preprocessFile (GL5_Path+"GL5\GL5_AddOns\GL5_Functions\GL5_Crew_FX_F.sqf");
				};
			};
		};

		case 11 :
		{
			if (GL5_Local select 77) then
			{
				call compile preprocessFile (GL5_Path+"GL5\GL5_AddOns\GL5_Functions\GL5_Backblast_FX_F.sqf");

				[] execVM (GL5_Path+"GL5\GL5_AddOns\GL5_Backblast\GL5_Backblast.sqf");

				if (_a) then
				{
					GL5_Local set [77, True];
				}
				else
				{
					GL5_Local set [77, False];
				};
			};
		};

		case 12 :
		{
			if (GL5_Local select 80) then
			{
				[_c] call compile preprocessFile (GL5_Path+"GL5\GL5_AddOns\GL5_Database\GL5_Initialize.sqf");

				call compile preprocessFile (GL5_Path+"GL5\GL5_AddOns\GL5_Functions\GL5_Artillery_FX_F.sqf");
			};
		};

		case 13 :
		{
			if (GL5_Local select 82) then
			{
				[] execVM (GL5_Path+"GL5\GL5_AddOns\GL5_Autumn\GL5_Autumn.sqf");
			};
		};

		case 14 :
		{
			_d = isClass (configFile >> "cfgPatches" >> "GL5_Config_FX");

			if (_d) then
			{
				GL5_Config_FX = [ "\GL5_Config_FX\GL5_Sparks\GL5_Sparks.p3d" ];
			}
			else
			{
				GL5_Config_FX = [ ["\A3\data_f\ParticleEffects\Universal\Universal", 16, 13, 2, 0] ];
			};
		};
	};

	_c = _c + 1;
};