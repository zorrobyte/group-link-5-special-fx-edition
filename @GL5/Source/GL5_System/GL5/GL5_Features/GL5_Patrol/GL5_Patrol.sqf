// ////////////////////////////////////////////////////////////////////////////
// GL5 v.1.0
// ////////////////////////////////////////////////////////////////////////////
// Patrol
// Script by =\SNKMAN/=
// ////////////////////////////////////////////////////////////////////////////
private ["_a","_b","_c","_d","_e"];

_a = (GL5_Groups select 0);

if (count _a > 0) then
{
	_b = 0;

	while { (_b < count _a) } do
	{
		_c = (_a select _b);

		_d = (vehicle leader _c);
		
		_e = 0;//test

		_e = _d getVariable "GL5_Patrol";

		if (typeName _e != "String") then
		{
			GL5_Patrol set [0, (GL5_Patrol select 0) + [_c] ];

			if (typeName _e != "Array") then
			{
				_e = [_e];
			};

			[_c, _e] spawn (GL5_Patrol_F select 0);
		};

		_b = _b + 1;
	};
};