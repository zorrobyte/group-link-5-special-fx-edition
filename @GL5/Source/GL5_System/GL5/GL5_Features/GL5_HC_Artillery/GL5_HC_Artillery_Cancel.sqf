// ////////////////////////////////////////////////////////////////////////////
// GL5 v.1.0
// ////////////////////////////////////////////////////////////////////////////
// High Command Artillery Cancel
// 
// ////////////////////////////////////////////////////////////////////////////
private ["_a"];

_a = _this select 0;

if (local _a) then
{
	GL5_HC_Artillery_Menu set [2, ["Cancel", [3], "", -5, [ ["expression", "[player] execVM (GL5_Path+""GL5\GL5_Features\GL5_HC_Artillery\GL5_HC_Artillery_Cancel.sqf"") "] ], "0", "0"] ];

	call compile format [" [playerSide, ""HQ""] sidechat""%1 artillery cancel confirmed. We will stop the requested artillery support."" ", name _a];
};

if (isMultiplayer) then
{
	GL5_HC_Artillery_Server_PublicVariable = [3, _a]; publicVariable "GL5_HC_Artillery_Server_PublicVariable";
};

if (isServer) then
{
	GL5_HC_Artillery set [5, False];

	deleteMarker GL5_HC_Artillery_Marker;

	GL5_HC_Artillery_Marker = nil;
};