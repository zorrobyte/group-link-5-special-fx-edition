// ////////////////////////////////////////////////////////////////////////////
// GL5 v.1.0
// ////////////////////////////////////////////////////////////////////////////
// High Command Force Move Regroup
// 
// ////////////////////////////////////////////////////////////////////////////
private ["_a","_b","_c"];

_a = _this select 0;
_b = _this select 1;

onMapSingleClick "";

if (typeName _b == "Object") then
{
	GL5_HC_Force_Move set [1, (GL5_HC_Force_Move select 1) - [_b] ];

	if (alive _b) then
	{
		_b setUnitPos "AUTO";

		_b setSpeedMode (speedMode _a);

		_b setCombatMode (combatMode _a);

		_b setBehaviour (behaviour _a);

		_b enableAI "TARGET";

		_b enableAI "AUTOTARGET";

		[_b] join _a;
	};
}
else
{
	while { (count _b > 0) } do
	{
		_c = (_b select 0);

		GL5_HC_Force_Move set [1, (GL5_HC_Force_Move select 1) - [_c] ];

		if (alive _c) then
		{
			_c setUnitPos "AUTO";

			_c setSpeedMode (speedMode _a);

			_c setCombatMode (combatMode _a);

			_c setBehaviour (behaviour _a);

			_c enableAI "TARGET";

			_c enableAI "AUTOTARGET";

			[_c] join _a;
		};

		_b = _b - [_c];
	};
};

if (count (GL5_HC_Force_Move select 1) == 0) then
{
	if (_a getVariable "GL5_HC_Force_Move") then
	{
		_a setVariable ["GL5_HC_Force_Move", False];
	}
	else
	{
		_a setVariable ["GL5_HC_Force_Move", True];
	};

	deleteMarker GL5_HC_Force_Move_Marker;

	GL5_HC_Force_Move_Marker = nil;

	GL5_HC_Force_Move_Menu set [3, ["Regroup ( Leader )", [4], "", -5, [ ["expression", "[player, (GL5_HC_Force_Move select 1) ] execVM (GL5_Path+""GL5\GL5_Features\GL5_HC_Force_Move\GL5_HC_Force_Move_Regroup.sqf"") "] ], "0", "0"] ];
};