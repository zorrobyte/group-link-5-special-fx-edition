// ////////////////////////////////////////////////////////////////////////////
// GL5 v.1.0
// ////////////////////////////////////////////////////////////////////////////
// High Command Reinforcement Server
// 
// ////////////////////////////////////////////////////////////////////////////
private ["_a","_b","_c"];

_a = _this select 0;
_b = _this select 1;

if (isServer) then
{
	switch (_a) do
	{
		case 1 :
		{
			[_b] execVM (GL5_Path+"GL5\GL5_Features\GL5_HC_Reinforcement\GL5_HC_Reinforcement_Request.sqf");
		};

		case 2 :
		{
			_c = _this select 2;

			[_b, _c] execVM (GL5_Path+"GL5\GL5_Features\GL5_HC_Reinforcement\GL5_HC_Reinforcement_Map.sqf");
		};

		case 3 :
		{
			[_b] execVM (GL5_Path+"GL5\GL5_Features\GL5_HC_Reinforcement\GL5_HC_Reinforcement_Status.sqf");
		};

		case 4 :
		{
			[_b] execVM (GL5_Path+"GL5\GL5_Features\GL5_HC_Reinforcement\GL5_HC_Reinforcement_Cancel.sqf");
		};
	};
};