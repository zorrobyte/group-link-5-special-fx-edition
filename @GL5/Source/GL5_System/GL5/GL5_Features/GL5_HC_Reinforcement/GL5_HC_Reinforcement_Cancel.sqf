// ////////////////////////////////////////////////////////////////////////////
// GL5 v.1.0
// ////////////////////////////////////////////////////////////////////////////
// High Command Reinforcement Cancel
// 
// ////////////////////////////////////////////////////////////////////////////
private ["_a"];

_a = _this select 0;

if (local _a) then
{
	GL5_HC_Reinforcement_Menu set [2, ["Command Map", [3], "", -5, [ ["expression", "[player] execVM (GL5_Path+""GL5\GL5_Features\GL5_HC_Reinforcement\GL5_HC_Reinforcement_Map.sqf"") "] ], "0", "0"] ];

	GL5_HC_Reinforcement_Menu set [3, ["Status", [4], "", -5, [ ["expression", "[player] execVM (GL5_Path+""GL5\GL5_Features\GL5_HC_Reinforcement\GL5_HC_Reinforcement_Status.sqf"") "] ], "0", "0"] ];

	GL5_HC_Reinforcement_Menu set [4, ["Cancel", [5], "", -5, [ ["expression", "[player] execVM (GL5_Path+""GL5\GL5_Features\GL5_HC_Reinforcement\GL5_HC_Reinforcement_Cancel.sqf"") "] ], "0", "0"] ];

	call compile format [" [playerSide, ""HQ""] sidechat""%1 reinforcement cancel confirmed. We will retreat the requested reinforcement."" ", name _a];

	onMapSingleClick "";
};

if (isMultiplayer) then
{
	GL5_HC_Reinforcement_Server_PublicVariable = [4, _a]; publicVariable "GL5_HC_Reinforcement_Server_PublicVariable";
};

if (isServer) then
{
	GL5_HC_Reinforcement set [4, False];

	deleteMarker GL5_HC_Reinforcement_Marker;

	GL5_HC_Reinforcement_Marker = nil;

	sleep 5;

	[ (GL5_HC_Reinforcement select 2) ] call (GL5_Remount_F select 0);

	GL5_HC_Reinforcement set [2, [] ];

	GL5_HC_Reinforcement set [5, True];
};