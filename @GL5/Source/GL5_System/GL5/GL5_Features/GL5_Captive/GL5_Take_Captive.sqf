// ////////////////////////////////////////////////////////////////////////////
// GL5 v.1.0
// ////////////////////////////////////////////////////////////////////////////
// Captive
// Script by =\SNKMAN/=
// ////////////////////////////////////////////////////////////////////////////
private ["_a","_b","_c","_d","_e","_f","_g"];

_a = _this select 0;
_b = _this select 2;

_c = player;

if (count (GL5_Captive select 0) <= (GL5_Global select 56) ) then
{
	GL5_Captive set [0, (GL5_Captive select 0) + [_a] ];

	_d = format ["%1 was taken captive.", name _a];

	if (isMultiplayer) then
	{
		GL5_Captive_Player_PublicVariable = [3, _a, _b, _c, _d]; publicVariable "GL5_Captive_Player_PublicVariable";

		_c groupChat _d;

		_a removeAction _b;
	}
	else
	{
		_c groupChat _d;

		_a removeAction _b;
	};

	_e = _a addAction [format ["%1 Interrogate", name _a], (GL5_Path+"GL5\GL5_Features\GL5_Captive\GL5_Interrogate_Captive.sqf"), _c];

	_f = _a addAction [format ["%1 Stop", name _a], (GL5_Path+"GL5\GL5_Features\GL5_Captive\GL5_Stop_Captive.sqf"), _c];

	_g = _a addAction [format ["%1 Release", name _a], (GL5_Path+"GL5\GL5_Features\GL5_Captive\GL5_Release_Captive.sqf"), _c];

	GL5_Captive set [1, (GL5_Captive select 1) + [_a, _f] ];

	if (local _a) then
	{
		[1, _a, _c] execVM (GL5_Path+"GL5\GL5_Features\GL5_Captive\GL5_Captive_Server.sqf");
	}
	else
	{
		GL5_Captive_Server_PublicVariable = [1, _a, _c]; publicVariable "GL5_Captive_Server_PublicVariable";
	};

	while { ( (alive _a) && { (alive _c) } && { (captive _a) } ) } do
	{
		sleep 1;
	};

	{_a removeAction _x} count [_e, _f, _g];

	[5, _a] execVM (GL5_Path+"GL5\GL5_Features\GL5_Captive\GL5_Captive_Player.sqf");
}
else
{
	hint format ["%1 you are not allowed to take more captives.", name _c];
};