// ////////////////////////////////////////////////////////////////////////////
// GL5 v.1.0
// ////////////////////////////////////////////////////////////////////////////
// Recruit
// Script by =\SNKMAN/=
// ////////////////////////////////////////////////////////////////////////////
private ["_a","_b","_c"];

_a = _this select 0;
_b = _this select 2;

_c = group _a;

[_a] join grpNull;

if (isMultiplayer) then
{
	GL5_Recruit_Server_PublicVariable = [1, _a, _c]; publicVariable "GL5_Recruit_Server_PublicVariable";
}
else
{
	[1, _a, _c] execVM (GL5_Path+"GL5\GL5_Features\GL5_Recruit\GL5_Recruit_Server.sqf");
};