// ////////////////////////////////////////////////////////////////////////////
// GL5 v.1.0
// ////////////////////////////////////////////////////////////////////////////
// AddOns Initialize
// 
// ////////////////////////////////////////////////////////////////////////////
private ["_a","_b","_c"];

_a = _this select 0;

switch (_a) do
{
	case 0 :
	{
		GL5_Reloading = [ [], [] ];
	};

	case 1 :
	{
		GL5_Blood = [ [], [], True, True, 180];

		GL5_Bleeding = [ [], True, True, True ];

		GL5_Texture_F = compile preprocessFile (GL5_Path+"GL5\GL5_AddOns\GL5_Blood\GL5_Texture.sqf");

		_a = 0;

		{_a = _a + _x; GL5_Blood set [1, (GL5_Blood select 1) + [_a] ]} count [0.05, 0.225, 0.325, 0.4];
	};

	case 3 :
	{
		GL5_Sound_FX = [

			[],
			[],

			call {private ["_a","_b","_c"];

			_a = [];

			_b = 0;

			while { (_b < 50) } do
			{
				_c = "Logic" createVehicleLocal [0,0,0];

				_a = _a + [_c];

				_b = _b + 1;
			};

			_a

			},

			[]
		];

		[] execVM (GL5_Path+"GL5\GL5_AddOns\GL5_Ambient\GL5_Church.sqf");

		[] execVM (GL5_Path+"GL5\GL5_AddOns\GL5_Ambient\GL5_Lighthouse.sqf");
	};

	case 4 :
	{
		GL5_Weapon_FX = [ "Logic" createVehicleLocal [0,0,0] ];
	};

	case 5 :
	{
		GL5_Shell_FX = [ "Logic" createVehicleLocal [0,0,0] ];
	};

	case 7 :
	{
		GL5_Explosion_FX = [ "Logic" createVehicleLocal [0,0,0], 0, [] ];

		call compile preprocessFile (GL5_Path+"GL5\GL5_Functions\GL5_FX_F\GL5_Debri_FX_F.sqf");

		call compile preprocessFile (GL5_Path+"GL5\GL5_Functions\GL5_FX_F\GL5_Shock_FX_F.sqf");
	};

	case 8 :
	{
		if (isNil "GL5_Fire_FX_F") then
		{
			call compile preprocessFile (GL5_Path+"GL5\GL5_Functions\GL5_FX_F\GL5_Fire_FX_F.sqf");
		};
	};

	case 9 :
	{
		GL5_Burning_FX = [ [] ];

		if (isNil "GL5_Fire_FX_F") then
		{
			call compile preprocessFile (GL5_Path+"GL5\GL5_Functions\GL5_FX_F\GL5_Fire_FX_F.sqf");
		};
	};

	case 10 :
	{
		if (isNil "GL5_Fire_FX_F") then
		{
			call compile preprocessFile (GL5_Path+"GL5\GL5_Functions\GL5_FX_F\GL5_Fire_FX_F.sqf");
		};
	};

	case 12 :
	{
		GL5_Artillery_FX = [ "Logic" createVehicleLocal [0,0,0] ];
	};
};