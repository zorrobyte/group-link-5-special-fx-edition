// ////////////////////////////////////////////////////////////////////////////
// GL5 v.1.0
// ////////////////////////////////////////////////////////////////////////////
// Dubbing
//
// ////////////////////////////////////////////////////////////////////////////
private ["_a","_b","_c","_d","_e"];

waitUntil { (GL5_Initialize select 1) };

sleep 5 + (random 10);

if (GL5_Local select 1) then
{
	_a = call compile preProcessFile (GL5_Path+"GL5\GL5_AddOns\GL5_Dubbing\GL5_Dubbing_Select.sqf");

	while { (count (GL5_Dubbing select 0) > 0) } do
	{
		_b = (GL5_Dubbing select 0);

		_c = 0;

		while { (_c < count _b) } do
		{
			_d = (_b select _c);

			if ( { (alive _x) } count (units _d) > 1) then
			{
				_e = (vehicle leader _d);

				if ( { ( (_e isKindOf "CaManBase") && (_e distance _x < (GL5_Local select 2) ) ) } count (GL5_Player select 0) > 0) then
				{
					if (floor (random 100) < _a) then
					{
						[_d] spawn (GL5_Dubbing_F select 0);
					};
				};
			};

			_c = _c + 1;
		};

		sleep 1;
	};
};