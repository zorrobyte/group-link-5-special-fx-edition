// ////////////////////////////////////////////////////////////////////////////
// GL5 v.1.0
// ////////////////////////////////////////////////////////////////////////////
// Backblast
// 
// ////////////////////////////////////////////////////////////////////////////
private ["_a","_b","_c","_d"];

_a = allUnits;

_b = 0;

while { (_b < count _a) } do
{
	_c = (_a select _b);

	_d = (weapons _c);

	if ( { (_x in (GL5_Resource select 42) ) } count _d > 0) then
	{
		[_c] spawn (GL5_Backblast_FX_F select 5);
	};

	_b = _b + 1;
};