// ////////////////////////////////////////////////////////////////////////////
// GL5 v.1.0
// ////////////////////////////////////////////////////////////////////////////
// Autumn
// 
// ////////////////////////////////////////////////////////////////////////////
private ["_a","_b","_c","_d","_e","_leafs"];

call compile preprocessFile (GL5_Path+"GL5\GL5_System\GL5_Join_In_Progress.sqf");

_a = player;

while { (GL5_Local select 82) } do
{
	if ( !(surfaceIsWater getPosATL _a) && (getPosATL _a select 2 < 20) ) then
	{
		_b = Wind;

		if ( (_b select 2) <= 0) then
		{
			_b set [2, random 1];
		};

		_c = (GL5_Local select 83);

		_c = _c + (random _c);

		_d = 0;

		while { (_d < _c) } do
		{
			_e = [ [ (getPosATL _a select 0) + ((random 50) - (random 50)), (getPosATL _a select 1) + ((random 50) - (random 50)), (getPosATL _a select 2) ],
			       [ (_b select 0) * 2, (_b select 1) * 2, random (_b select 2) ],
			       0.7 + random 0.7
			];

			_leafs = ["\a3\data_f\cl_leaf.p3d","\a3\data_f\cl_leaf2.p3d","\a3\data_f\cl_leaf3.p3d","\a3\data_f\ParticleEffects\Hit_Leaves\Leaves.p3d","\a3\data_f\ParticleEffects\Hit_Leaves\Leaves_Green.p3d"] call GL5_Random_Select_F;

			drop [ _leafs, "", "SpaceObject", 1, _c,
 
			(_e select 0), (_e select 1), (_e select 2), 1.3 + random 0.05, 1, 0.2,

			[0.7 + random 0.7], [ [1,1,1,1] ], [0,1],

			0.05, 0.05, "", "", ""];

			_d = _d + 1;
		};
	}
	else
	{
		_c = (GL5_Local select 83);
	};

	_c = _c - (random _c);

	sleep _c;
};