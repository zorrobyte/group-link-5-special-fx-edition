// ////////////////////////////////////////////////////////////////////////////
// GL5 v.1.0
// ////////////////////////////////////////////////////////////////////////////
// Chatter Assign
// 
// ////////////////////////////////////////////////////////////////////////////
private ["_a"];

_a = _this select 0;

while { (count (units _a) == 1) } do
{
	sleep 1;
};

if (alive _a) then
{
	[_a] call (GL5_Chatter_F select 0);

	call compile preprocessFile "\a3\functions_f\Misc\fn_commsMenuCreate.sqf";

	GL5_HQ_Radio_Menu = [

		["H.Q. Radio", False],
		["On", [2], "", -5, [ ["expression", " [1, (groupSelectedUnits player) ] call (GL5_Chatter_F select 1) "] ], "1", "1"],
		["Off", [3], "", -5, [ ["expression", " [2, (groupSelectedUnits player) ] call (GL5_Chatter_F select 1) "] ], "1", "1"]
	];

	if (isNil "GL5_Array_Push_F") then
	{
		call compile preprocessFile (GL5_Path+"GL5\GL5_Functions\GL5_Extension_F.sqf");
	};

	[BIS_MENU_GroupCommunication, ["", [], "", -1, [], "0", "0"] ] call GL5_Array_Push_F;

	[BIS_MENU_GroupCommunication, ["H.Q. Radio", [0], "#User:GL5_HQ_Radio_Menu", -5, [ ["expression", ""] ], "1", "1"] ] call GL5_Array_Push_F;
};