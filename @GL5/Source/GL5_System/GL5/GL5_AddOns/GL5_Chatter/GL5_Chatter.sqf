// ////////////////////////////////////////////////////////////////////////////
// GL5 v.1.0
// ////////////////////////////////////////////////////////////////////////////
// Radio Chatter
// 
// ////////////////////////////////////////////////////////////////////////////
private ["_a","_b","_c"];

call compile preprocessFile (GL5_Path+"GL5\GL5_Database\GL5_Radio_Chatter.sqf");

call compile preprocessFile (GL5_Path+"GL5\GL5_System\GL5_Join_In_Progress.sqf");

(findDisplay 46) displayAddEventHandler ["KeyDown", "_this call (GL5_Chatter_F select 5)"];

if (player == leader player) then
{
	[player] execVM (GL5_Path+"GL5\GL5_AddOns\GL5_Chatter\GL5_Chatter_Assign.sqf");
};

while { (GL5_Local select 22) } do
{
	sleep 10;

	_a = (vehicle player);

	_b = getNumber (configFile >> "CfgVehicles" >> (typeOf _a) >> "side");

	switch (_b) do
	{
		case 0 :
		{
			_a = (vehicle player);

			if ( (_a isKindOf "Tank") && { (isEngineOn _a) } && { (GL5_Chatter select 1) } ) then
			{
				[ (GL5_Chatter select 2) ] call (GL5_Chatter_F select 7);

				while { ( (_a isKindOf "Tank") && { (isEngineOn _a) } && { (GL5_Chatter select 1) } ) } do
				{
					_c = (GL5_Resource select 22) call GL5_Random_Select_F;

					0 fadeSound (GL5_Chatter select 2);

					playSound _c;

					sleep 30 - (random 30);

					_a = (vehicle player);
				};
			}
			else
			{
				if ( (_a isKindOf "LandVehicle") && { (isEngineOn _a) } && { (GL5_Chatter select 1) } ) then
				{
					[ (GL5_Chatter select 2) ] call (GL5_Chatter_F select 7);

					while { ( (_a isKindOf "Wheeled_APC") && (isEngineOn _a) && (GL5_Chatter select 1) ) } do
					{
						_c = (GL5_Resource select 22) call GL5_Random_Select_F;

						0 fadeSound (GL5_Chatter select 2);

						playSound _c;

						sleep 30 - (random 30);

						_a = (vehicle player);
					};
				};
			};
		};

		case 1 :
		{
			_a = (vehicle player);

			if ( (_a isKindOf "Air") && { (isEngineOn _a) } && { (GL5_Chatter select 1) } ) then
			{
				[ (GL5_Chatter select 2) ] call (GL5_Chatter_F select 7);

				while { ( (_a isKindOf "Air") && (isEngineOn _a) && (GL5_Chatter select 1) ) } do
				{
					if (floor (random 100) < 99) then
					{
						_c = (GL5_Resource select 20) + (GL5_Resource select 23) call GL5_Random_Select_F;

						0 fadeSound (GL5_Chatter select 2);

						playSound _c;

						sleep 30 - (random 30);
					}
					else
					{
						if (floor (random 100) < 50) then
						{
							[_a] call (GL5_Chatter_F select 3);

							sleep 30 - (random 30);
						}
						else
						{
							[_a] call (GL5_Chatter_F select 4);

							sleep 30 - (random 30);
						};
					};

					_a = (vehicle player);
				};
			}
			else
			{
				if ( (_a isKindOf "Tank") && { (isEngineOn _a) } && { (GL5_Chatter select 1) } ) then
				{
					[ (GL5_Chatter select 2) ] call (GL5_Chatter_F select 7);

					while { ( (_a isKindOf "Tank") && (isEngineOn _a) && (GL5_Chatter select 1) ) } do
					{
						_c = (GL5_Resource select 21) + (GL5_Resource select 23) call GL5_Random_Select_F;

						0 fadeSound (GL5_Chatter select 2);

						playSound _c;

						sleep 30 - (random 30);

						_a = (vehicle player);
					};
				}
				else
				{
					if ( ( (_a isKindOf "LandVehicle") || (_a isKindOf "LandVehicle") ) && { (isEngineOn _a) } && { (GL5_Chatter select 1) } ) then
					{
						[ (GL5_Chatter select 2) ] call (GL5_Chatter_F select 7);

						while { ( ( (_a isKindOf "Wheeled_APC") || (_a isKindOf "HMMWV_Base") ) && (isEngineOn _a) && (GL5_Chatter select 1) ) } do
						{
							_c = (GL5_Resource select 21) + (GL5_Resource select 23) call GL5_Random_Select_F;

							0 fadeSound (GL5_Chatter select 2);

							playSound _c;

							sleep 30 - (random 30);

							_a = (vehicle player);
						};
					};
				};
			};
		};

		case 2 :
		{
			_a = (vehicle player);

			if ( (_a isKindOf "Tank") && { (isEngineOn _a) } && { (GL5_Chatter select 1) } ) then
			{
				[ (GL5_Chatter select 2) ] call (GL5_Chatter_F select 7);

				while { ( (_a isKindOf "Tank") && (isEngineOn _a) && (GL5_Chatter select 1) ) } do
				{
					_c = (GL5_Resource select 22) call GL5_Random_Select_F;

					0 fadeSound (GL5_Chatter select 2);

					playSound _c;

					sleep 30 - (random 30);

					_a = (vehicle player);
				};
			}
			else
			{
				if ( (_a isKindOf "LandVehicle") && { (isEngineOn _a) } && { (GL5_Chatter select 1) } ) then
				{
					[ (GL5_Chatter select 2) ] call (GL5_Chatter_F select 7);

					while { ( (_a isKindOf "Wheeled_APC") && (isEngineOn _a) && (GL5_Chatter select 1) ) } do
					{
						_c = (GL5_Resource select 22) call GL5_Random_Select_F;

						0 fadeSound (GL5_Chatter select 2);

						playSound _c;

						sleep 30 - (random 30);

						_a = (vehicle player);
					};
				};
			};

		};
	};
};