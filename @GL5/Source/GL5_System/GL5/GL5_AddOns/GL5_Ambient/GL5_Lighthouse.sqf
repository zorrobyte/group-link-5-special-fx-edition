// ////////////////////////////////////////////////////////////////////////////
// GL5 v.1.0
// ////////////////////////////////////////////////////////////////////////////
// Lighthouse
// Idea and Sounds by Operation Flashpoint MOD E.C.P. ( Enhanced Configuration Project )
//
// ////////////////////////////////////////////////////////////////////////////
private ["_a","_b","_c"];

call compile preprocessFile (GL5_Path+"GL5\GL5_System\GL5_Join_In_Progress.sqf");

sleep 10 + (random 30);

while { (True) } do
{
	if !( (dayTime > 5.00) && (dayTime < 19.00) ) then
	{
		_a = (vehicle player);

		_b = nearestObjects [_a, ["Land_NAV_Lighthouse","Land_NAV_Lighthouse1","Land_NAV_Lighthouse2","Land_LightHouse_F","Land_Lighthouse_small_F"], 2500];

		if (count _b > 0) then
		{
			_c = (GL5_Resource select 35) call GL5_Random_Select_F;

			(_b select 0) say3D _c;

			sleep 240 + (random 240);
		}
		else
		{
			sleep 240 + (random 240);
		};
	}
	else
	{
		sleep 240 + (random 240);
	};
};