// ////////////////////////////////////////////////////////////////////////////
// GL5 v.1.0
// ////////////////////////////////////////////////////////////////////////////
// Church
// Idea and Sounds by Operation Flashpoint MOD E.C.P. ( Enhanced Configuration Project )
// 
// ////////////////////////////////////////////////////////////////////////////
private ["_a","_b","_c","_d","_e","_f"];

call compile preprocessFile (GL5_Path+"GL5\GL5_System\GL5_Join_In_Progress.sqf");

while { (True) } do
{
	if ( (dayTime > 6.00) && (dayTime < 19.00) ) then
	{
		sleep 10 + (random 30);

		_a = nearestObjects [player, ["Land_Church_01","Land_Church_02","Land_Church_03","Land_Chapel_Small_V1_F","Land_Chapel_Small_V2_F","Land_Chapel_V1_F","Land_Chapel_V2_F","Land_Church_01_V1_F"], 500];

		if ( (count _a > 0) && (round (random 100) < 75) ) then
		{
			_b = (GL5_Resource select 32) call GL5_Random_Select_F;
			_c = (GL5_Resource select 33) call GL5_Random_Select_F;
			_d = (GL5_Resource select 34) call GL5_Random_Select_F;

			_e = [_b, _c, _d] call GL5_Random_Select_F;

			_f = 60 + (random 60);

			switch (_e) do
			{
				case _b :
				{
					_f = 120 + (random 120);
				};
				case _c :
				{
					_f = 240 + (random 240);
				};
				case _d :
				{
					_f = 480 + (random 480);
				};
			};

			(_a select 0) say3D _e;

			sleep _f;
		}
		else
		{
			sleep 240 + (random 240);
		};
	}
	else
	{
		_a = nearestObjects [player, ["Land_Church_01","Land_Church_02","Land_Church_03","Land_Chapel_Small_V1_F","Land_Chapel_Small_V2_F","Land_Chapel_V1_F","Land_Chapel_V2_F","Land_Church_01_V1_F"], 2000];

		if ((count _a > 0))then
		{
			_c = 0;

			while { (_c < count _a) } do
			{
				_d = (_a select _c);

				_e = "#lightpoint" createVehicleLocal (getPosATL _d);

				_e setPosATL [ (getPosATL _d select 0), (getPosATL _d select 1), (getPosATL _d select 2) + 10];

				_e setLightBrightness 0.15;

				_f = [ [1, 2, 1], [2, 1, 0], [2, 2, 2] ] call GL5_Random_Select_F;

				_e setLightAmbient _f;
				_e setLightColor _f;

				_c = _c + 1;
			};
			
			if ((player distance _e) > 2500) then
			{
				deleteVehicle _e;
			};
		};

		sleep 240 + (random 240);
	};
};