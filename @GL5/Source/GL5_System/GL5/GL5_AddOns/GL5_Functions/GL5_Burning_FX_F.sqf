// ////////////////////////////////////////////////////////////////////////////
// GL5 v.1.0
// ////////////////////////////////////////////////////////////////////////////
// Burning FX Functions
// 
// ////////////////////////////////////////////////////////////////////////////

GL5_Burning_FX_F = [

	// ////////////////////////////////////////////////////////////////////////////
	// Burning FX Function #0
	// ////////////////////////////////////////////////////////////////////////////
	// Burning FX
	// 
	// ////////////////////////////////////////////////////////////////////////////
	{private ["_a","_b"];

	_a = _this select 0;

	if (GL5_Local select 74) then
	{
		_b = (sizeOf typeOf _a);

		_b = _b * 15;

		[_a, _b] spawn (GL5_Fire_FX_F select 1);

		[_a, _b] spawn (GL5_Burning_FX_F select 1);
	};

	},

	// ////////////////////////////////////////////////////////////////////////////
	// Burning FX Function #1
	// ////////////////////////////////////////////////////////////////////////////
	// Burning FX
	// 
	// ////////////////////////////////////////////////////////////////////////////
	{private ["_a","_b","_c","_d","_e","_f","_g"];

	_a = _this select 0;
	_b = _this select 1;

	_c = time + _b;

	_d = time + 3;

	while { (time < _c) } do
	{
		_e = (vehicle player);

		if ( (_e isKindOf "CaManBase") && (_e distance _a < 50) ) then
		{
			_f = (_e distance _a);
			_g = False;

			if (_f <= 10) then
			{
				"dynamicBlur" ppEffectEnable True;

				if (_e in (GL5_Burning_FX select 0) ) then
				{
					_g = False;
				}
				else
				{
					_g = True;

					if (time > _d) then
					{
						_d = time + 3;

						titleRsc ["GL5_Burning", "PLAIN", 2];

						"dynamicBlur" ppEffectAdjust [5 / _f];
						"dynamicBlur" ppEffectCommit 1;

						_e setDammage (getDammage _e + 0.01 / _f);
					};
				};

				if ( (_f < 5) && (_g) ) then
				{
					GL5_Burning_FX set [0, (GL5_Burning_FX select 0) + [_e] ];

					if (isMultiplayer) then
					{
						GL5_AddOn_PublicVariable = [5, _e]; publicVariable "GL5_AddOn_PublicVariable";

						[_e] spawn (GL5_Fire_FX_F select 2);
					}
					else
					{
						[_e] spawn (GL5_Fire_FX_F select 2);
					};
				};
			}
			else
			{
				if (_g) then
				{
					if ( (_f > 10) && (_f <= 15) ) then
					{
						"dynamicBlur" ppEffectAdjust [3 / _f];
						"dynamicBlur" ppEffectCommit 1;
					}
					else
					{
						if ( (_f > 15) && (_f <= 20) ) then
						{
							"dynamicBlur" ppEffectAdjust [0];
							"dynamicBlur" ppEffectCommit 1;
						};
					};
				};
			};

			sleep 1;
		}
		else
		{
			sleep 10;
		};
	};

	"dynamicBlur" ppEffectEnable False;

	},

	// ////////////////////////////////////////////////////////////////////////////
	// Burning FX Function #2
	// ////////////////////////////////////////////////////////////////////////////
	// Burning FX
	// 
	// ////////////////////////////////////////////////////////////////////////////
	{private ["_a","_b","_c","_d","_e"];

	_a = _this select 0;

	_b = time + 3;

	while { !(isNull _a) } do
	{
		if (inflamed _a) then
		{
			while { (inflamed _a) } do
			{
				_c = (vehicle player);

				if ( (_c isKindOf "CaManBase") && { (alive _c) } && { (_c distance _a < 50) } ) then
				{
					_d = (_c distance _a);

					if (_d <= 3) then
					{
						"dynamicBlur" ppEffectEnable True;

						if (_c in (GL5_Burning_FX select 0) ) then
						{
							_e = False;
						}
						else
						{
							_e = True;

							if (time > _b) then
							{
								_b = time + 3;

								titleRsc ["GL5_Burning", "PLAIN", 2];

								"dynamicBlur" ppEffectAdjust [1 / _d];
								"dynamicBlur" ppEffectCommit 1;

								_c setDammage (getDammage _c + 0.01 / _d);
							};
						};
					}
					else
					{
						if (_e) then
						{
							if ( (_d > 3) && (_d <= 5) ) then
							{
								"dynamicBlur" ppEffectAdjust [1 / _d];
								"dynamicBlur" ppEffectCommit 1;
							}
							else
							{
								if ( (_d > 5) && (_d <= 10) ) then
								{
									"dynamicBlur" ppEffectAdjust [0];
									"dynamicBlur" ppEffectCommit 1;
								};
							};
						};
					};

					sleep 1;
				}
				else
				{
					sleep 10;
				};
			};

			"dynamicBlur" ppEffectEnable False;
		}
		else
		{
			sleep 10;
		};
	};

	}
];