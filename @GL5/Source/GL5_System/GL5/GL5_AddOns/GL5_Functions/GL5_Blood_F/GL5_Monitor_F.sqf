// ////////////////////////////////////////////////////////////////////////////
// GL5 v.1.0
// ////////////////////////////////////////////////////////////////////////////
// Monitor Functions
// 
// ////////////////////////////////////////////////////////////////////////////

GL5_Monitor_F = [

	// ////////////////////////////////////////////////////////////////////////////
	// Monitor Function #0
	// ////////////////////////////////////////////////////////////////////////////
	// Monitor Delete
	// 
	// ////////////////////////////////////////////////////////////////////////////
	{private ["_a","_b","_c","_d"];

	GL5_Blood set [2, False ];

	_a = (GL5_Local select 9);

	_b = _a / 2;

	while { (count (GL5_Blood select 0) > _b) } do
	{
		_c = (GL5_Blood select 0);

		_d = (_c select 0);

		hideBody _d;

		sleep 0.01;

		GL5_Blood set [0, (GL5_Blood select 0) - [_d] ];

		deleteVehicle _d;
	};

	GL5_Blood set [2, True];

	},

	// ////////////////////////////////////////////////////////////////////////////
	// Monitor Function #1
	// ////////////////////////////////////////////////////////////////////////////
	// Monitor Time
	// 
	// ////////////////////////////////////////////////////////////////////////////
	{private ["_a","_b","_c"];

	GL5_Blood set [3, False ];

	if (count (GL5_Blood select 0) > 0) then
	{
		GL5_Blood set [4, time + 180];

		_a = (GL5_Blood select 0);

		_b = 0;

		while { (_b < count _a) } do
		{
			_c = (_a select _b);

			hideBody _c;

			sleep 0.1;

			GL5_Blood set [0, (GL5_Blood select 0) - [_c] ];

			deleteVehicle _c;

			_b = _b + 1;
		};
	};

	GL5_Blood set [3, True];

	}
];