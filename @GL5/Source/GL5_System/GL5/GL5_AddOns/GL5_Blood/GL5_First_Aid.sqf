// ////////////////////////////////////////////////////////////////////////////
// GL5 v.1.0
// ////////////////////////////////////////////////////////////////////////////
// First Aid
// 
// ////////////////////////////////////////////////////////////////////////////
private ["_a","_b","_c","_stance"];

_a = _this select 0;
_b = _this select 2;

_a removeAction _b;

GL5_Bleeding set [1, False];

//if (canStand _a) then
//{
//	_a switchMove "AinvPknlMstpSlayWrflDnon_medic";
//}
//else
//{
//	_a switchMove "AinvPknlMstpSlayWrflDnon_healed";
//};

//GET UNIT/PLAYER CURRENT STANCE
_stance= stance _a;

switch _stance do
	{
		case "STAND": 
			{
			_a playMoveNow "AinvPknlMstpSlayWrflDnon_medic";
			};
		case "CROUCH":
			{
			_a playMoveNow "AinvPknlMstpSlayWrflDnon_medic";
			};
		case "PRONE":
			{
			_a playMoveNow "AinvPpneMstpSlayWrflDnon_medic";
			};	
		default 
			{
			_a playMoveNow "AinvPknlMstpSlayWrflDnon_medic";
			};
	};

_c = 2 + (random 1) * acctime;

"dynamicBlur" ppEffectAdjust [getDammage _a / 2];
"dynamicBlur" ppEffectCommit _c;

sleep _c;

GL5_Bleeding set [0, (GL5_Bleeding select 0) - [_a] ];