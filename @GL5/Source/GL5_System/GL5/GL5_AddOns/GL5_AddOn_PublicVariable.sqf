// ////////////////////////////////////////////////////////////////////////////
// GL5 v.1.0
// ////////////////////////////////////////////////////////////////////////////
// FX Public Variable
// Script by =\SNKMAN/=
// ////////////////////////////////////////////////////////////////////////////
private ["_a","_b","_c"];

_a = _this select 0;

switch (_a) do
{
	case 1 :
	{
		if (GL5_Local select 1) then
		{
			_b = _this select 1;
			_c = _this select 2;

			_b say3D _c;
		};
	};

	case 2 :
	{
		if (GL5_Local select 4) then
		{
			_b = _this select 1;

			_b call (GL5_Blood_FX_F select 0);
		};
	};

	case 3 :
	{
		if (GL5_Local select 23) then
		{
			_b = _this select 1;
			_c = _this select 2;

			[_b, _c] spawn (GL5_Sound_FX_F select 3);
		};
	};

	case 4 :
	{
		if (GL5_Local select 42) then
		{
			_b = _this select 1;
			_c = _this select 2;

			[_b, _c] call (GL5_Impact_FX_F select 0);
		};
	};

	case 5 :
	{
		_b = _this select 1;

		if (_b isKindOf "CaManBase") then
		{
			if (GL5_Local select 74) then
			{
				[_b] spawn (GL5_Fire_FX_F select 2);
			};
		}
		else
		{
			_c = _this select 2;

			if (GL5_Local select 47) then
			{
				[_b, _c] call (GL5_Explosion_FX_F select 0);
			};

			if (GL5_Local select 66) then
			{
				[_b, _c] spawn (GL5_Wrack_FX_F select 0);
			};

			if (GL5_Local select 74) then
			{
 				[_b] spawn (GL5_Burning_FX_F select 0);
			};
		};
	};

	case 6 :
	{
		if (GL5_Local select 73) then
		{
			_b = _this select 1;
			_c = _this select 2;

			[_b, _c] call (GL5_House_FX_F select 0);
		};
	};

	case 7 :
	{
		if (GL5_Local select 80) then
		{
			_b = _this select 1;

			[_b] call (GL5_Artillery_FX_F select 0);
		};
	};

	case 8 :
	{
		if (GL5_Local select 87) then
		{
			_b = _this select 1;

			[_b] call (GL5_Debug_F select 1);
		};
	};
};