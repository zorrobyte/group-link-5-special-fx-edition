// ////////////////////////////////////////////////////////////////////////////
// GL5 v.1.0
// ////////////////////////////////////////////////////////////////////////////
// Reloading
//
// ////////////////////////////////////////////////////////////////////////////
private ["_a","_b","_c","_d"];

call compile preprocessFile (GL5_Path+"GL5\GL5_System\GL5_Join_In_Progress.sqf");

_a = (vehicle player);

if (_a == leader _a) then
{
	_b = 0;

	while { (_b < count units _a) } do
	{
		_c = (units _a select _b);

		_d = (vehicle _c);

		if !(_d in (GL5_Reloading select 0) ) then
		{
			GL5_Reloading set [0, (GL5_Reloading select 0) + [_d] ];

			[_d] spawn (GL5_Reloading_F select 0);
		};

		_b = _b + 1;
	};
};