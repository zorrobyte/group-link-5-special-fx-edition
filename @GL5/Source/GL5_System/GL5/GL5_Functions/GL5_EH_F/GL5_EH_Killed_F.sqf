// ////////////////////////////////////////////////////////////////////////////
// GL5 v.1.0
// ////////////////////////////////////////////////////////////////////////////
// Event Handler "Killed" Functions
//
// ////////////////////////////////////////////////////////////////////////////

GL5_EH_Killed_F = [

	// ////////////////////////////////////////////////////////////////////////////
	// Event Handler "Killed" Function #0
	// ////////////////////////////////////////////////////////////////////////////
	// Event Handler "Killed"
	// Script by =\SNKMAN/=
	// ////////////////////////////////////////////////////////////////////////////
	{private ["_a","_b","_c","_d"];

	_a = _this select 0;

	_b = (vehicle _a);

	if (isPlayer _a) then
	{
		GL5_Player set [0, (GL5_Player select 0) - [_a] ];

		[_a] execVM (GL5_Path+"GL5\GL5_Player\GL5_Spawn.sqf");
	};

	if (_b isKindOf "CaManBase") then
	{
		if !(isNil "GL5_Killed") then
		{
			GL5_Killed set [0, (GL5_Killed select 0) + [_b] ];
		};

		if (floor (random 100) < (GL5_Local select 7) ) then
		{
			if (isMultiplayer) then
			{
				if (isDedicated) then
				{
					GL5_AddOn_PublicVariable = [2, _this]; publicVariable "GL5_AddOn_PublicVariable";
				}
				else
				{
					GL5_AddOn_PublicVariable = [2, _this]; publicVariable "GL5_AddOn_PublicVariable";

					_this call (GL5_Blood_FX_F select 0);
				};
			}
			else
			{
				_this call (GL5_Blood_FX_F select 0);
			};
		};

		if (floor (random 100) < (GL5_Local select 28) ) then
		{
			if (floor (random 100) < 50) then
			{
				_c = "Head_Axis";
			}
			else
			{
				_c = "Leaning_Axis";
			};

			if (isMultiplayer) then
			{
				if (isDedicated) then
				{
					GL5_AddOn_PublicVariable = [3, _b, _c]; publicVariable "GL5_AddOn_PublicVariable";
				}
				else
				{
					GL5_AddOn_PublicVariable = [3, _b, _c]; publicVariable "GL5_AddOn_PublicVariable";

					[_b, _c] spawn (GL5_Sound_FX_F select 3);
				};
			}
			else
			{
				[_b, _c] spawn (GL5_Sound_FX_F select 3);
			};
		};
	}
	else
	{
		if ( (_a isKindOf "Car") || (_a isKindOf "Tank") || (_a isKindOf "Air") ) then
		{
			if (GL5_Global select 31) then
			{
				if (floor (random 100) < 50) then
				{
					[_a] call (GL5_Detection_F select 0);
				};
			};

			_c = [12] call GL5_Random_Chance_F;

			if (isDedicated) then
			{
				GL5_AddOn_PublicVariable = [5, _a, _c]; publicVariable "GL5_AddOn_PublicVariable";
			}
			else
			{
				[_a, _c] call (GL5_Explosion_FX_F select 0);

				[_a, _c] spawn (GL5_Wrack_FX_F select 0);

				[_a] spawn (GL5_Burning_FX_F select 0);
			};

			if (GL5_Local select 1) then
			{
				_d = _this select 1;

				if (_d countEnemy [_b] == 1) then
				{
				//	["Woohoo", _d] spawn (GL5_Dubbing_F select 4);
				};
			};

			if (GL5_Local select 75) then
			{
				if (count (crew _a) > 0) then
				{
					[_a] call (GL5_Crew_FX_F select 0);
				};
			};
		};
	};

	}
];