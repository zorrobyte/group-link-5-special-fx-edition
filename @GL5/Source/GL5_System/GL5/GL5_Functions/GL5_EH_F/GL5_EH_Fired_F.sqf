// ////////////////////////////////////////////////////////////////////////////
// GL5 v.1.0
// ////////////////////////////////////////////////////////////////////////////
// Event Handler "Fired" Functions
//
// ////////////////////////////////////////////////////////////////////////////

GL5_EH_Fired_F = [

	// ////////////////////////////////////////////////////////////////////////////
	// Event Handler "Fired" Function #0
	// ////////////////////////////////////////////////////////////////////////////
	// Event Handler "Fired" System
	// Script by =\SNKMAN/=
	// ////////////////////////////////////////////////////////////////////////////
	{private ["_a","_b","_c","_d","_e","_f"];

	_a = _this select 0;
	_b = _this select 1;
	_c = _this select 4;

	_d = (vehicle _a);

	_e = nearestObject [_d, _c];

	if (isServer) then
	{
		_f = (GL5_Player select 0) + (GL5_Player select 2);

		if (_d in _f) then
		{
			if (GL5_Global select 32) then
			{
				_this call (GL5_EH_Fired_F select 1);
			};

			if (GL5_Global select 42) then
			{
				_this call (GL5_EH_Fired_F select 2);
			};
		};

		if ( (_a getVariable "GL5_Smoke_Shell") && ( (_this select 2) == "SmokeShellMuzzle") ) then
		{
			_e setVeloCity [ (veloCity _e select 0) + ((sin (getDir _a)) + 3) * 2, (veloCity _e select 1) + ((cos (getDir _a)) + 3) * 2, (veloCity _e select 2) - 5 ];
		};
	};

	if (GL5_Local select 1) then
	{
		if (_d isKindOf "CaManBase") then
		{
			if ( (_b == "Throw") && (_c isKindOf "GrenadeHand") || (_c isKindOf "HandGrenade") ) then
			{
				[_d] call (GL5_Grenade_F select 0);
			};

			if (_c isKindOf "RocketBase") then
			{
				[_d, _e] spawn (GL5_Incoming_F select 0);
			};
		}
		else
		{
			if (_c isKindOf "ShellBase") then
			{
				[_d, _e] spawn (GL5_Incoming_F select 0);
			};
		};
	};

	if ( (GL5_Local select 4) && { (_d isKindOf "CaManBase") } && { (isPlayer _d) } && { (_c isKindOf "BulletBase") } && { (floor (random 100) < 35) } ) then
	{
		if (animationState _d != "AmovPpneMstpSrasWrflDnon") then
		{
			[_d, _e] call (GL5_Blood_FX_F select 5);
		};
	};

	if (GL5_Local select 23) then
	{
		if ( (GL5_Local select 24) && { (_c isKindOf "BulletBase") } && { (floor (random 100) < (GL5_Local select 25) ) } ) then
		{
			[_e] spawn (GL5_Sound_FX_F select 5);
		}
		else
		{
			if ( (GL5_Local select 26) && (_c isKindOf "ShellBase") ) then
			{
				[_e] spawn (GL5_Sound_FX_F select 0);
			};
		};
	};

	if (GL5_Local select 30) then
	{
		[_c, _d, _e] call (GL5_Weapon_FX_F select 0);
	};

	if ( (GL5_Local select 36) && { !(_d isKindOf "CaManBase") } && { (_c isKindOf "ShellBase") } )  then
	{
		[_d, _e] spawn (GL5_Shell_FX_F select 0);
	};

	if ( (GL5_Local select 77) && { (_d isKindOf "CaManBase") } && { ( (_c isKindOf "RocketBase") || (_c isKindOf "MissileBase") ) } ) then
	{
		_f = (getPosATL _e select 2);

		[_d, _e, _f] call (GL5_Backblast_FX_F select 0);
	};

	},

	// ////////////////////////////////////////////////////////////////////////////
	// Event Handler Function #1
	// ////////////////////////////////////////////////////////////////////////////
	// Event Handler "Fired" Advancing
	// Script by =\SNKMAN/=
	// ////////////////////////////////////////////////////////////////////////////
	{private ["_a","_b"];

	if (GL5_Advancing select 1) then
	{
		_this call (GL5_Advancing_F select 0);

		if (count (GL5_Advancing select 0) == 0) then
		{
			_a = _this select 0;

			_b = (GL5_Groups select 0);

			if (count _b > 0) then
			{
				[_a, _b] call (GL5_Advancing_F select 1);
			};
		};
	};

	},

	// ////////////////////////////////////////////////////////////////////////////
	// Event Handler "Fired" Function #2
	// ////////////////////////////////////////////////////////////////////////////
	// Event Handler "Fired" Suppressed Fire
	// Script by =\SNKMAN/=
	// ////////////////////////////////////////////////////////////////////////////
	{private ["_a","_b"];

	GL5_Suppressed set [1, time - (GL5_Suppressed select 3) ];

	if ( (GL5_Suppressed select 1) < (GL5_Global select 43) ) then
	{
		GL5_Suppressed set [2, (GL5_Suppressed select 2) + 1];
	}
	else
	{
		GL5_Suppressed set [2, 0];
	};

	if ( (GL5_Suppressed select 2) > (GL5_Global select 44) ) then
	{
		_a = _this select 0;

		_b = (GL5_Groups select 0);

		GL5_System set [3, _a];

		if (count _b > 0) then
		{
			[_a, _b] call (GL5_Suppressed_F select 0);
		};
	};

	GL5_Suppressed set [3, time];

	}
];