// ////////////////////////////////////////////////////////////////////////////
// GL5 v.1.0
// ////////////////////////////////////////////////////////////////////////////
// Advancing Functions
//
// ////////////////////////////////////////////////////////////////////////////

GL5_Advancing_F = [

	// ////////////////////////////////////////////////////////////////////////////
	// Advancing Function #0
	// ////////////////////////////////////////////////////////////////////////////
	// Advancing
	//
	// ////////////////////////////////////////////////////////////////////////////
	{private ["_a","_b","_c","_d","_e"];

	_a = _this select 0;
	_b = _this select 1;
	_c = _this select 2;
	_d = _this select 4;

	if (_b != "Put") then
	{
		if (_d in (GL5_Global select 33) ) exitWith
		{
			_e = nearestObject [_a, _d];

			[_a, _e] spawn (GL5_Advancing_F select 4);
		};

		if ( (_c in (GL5_Global select 36) ) || (_d in (GL5_Global select 36) ) ) then
		{
			GL5_Advancing set [2, 1];
		}
		else
		{
			if ( (_b in (GL5_Global select 35) ) || (_d in (GL5_Global select 35) ) ) then
			{
				GL5_Advancing set [2, 0];
			}
			else
			{
				if ( (_b in (GL5_Global select 36) ) || (_d in (GL5_Global select 36) ) ) then
				{
					GL5_Advancing set [2, 1];
				}
				else
				{
					if ( (_b in (GL5_Global select 37) ) || (_d in (GL5_Global select 37) ) ) then
					{
						GL5_Advancing set [2, 2];
					}
					else
					{
						GL5_Advancing set [2, 3];
					};
				};
			};
		};

		GL5_System set [3, _a];

		GL5_Advancing set [1, False];

		[] spawn
		{
			if (count (GL5_Advancing select 0) > 0) then
			{
				while { (count (GL5_Advancing select 0) > 0) } do
				{
					sleep 5;
				};
			}
			else
			{
				sleep 10;
			};

			GL5_Advancing set [1, True];
		};
	};

	},

	// ////////////////////////////////////////////////////////////////////////////
	// Advancing Function #1
	// ////////////////////////////////////////////////////////////////////////////
	// Advancing
	//
	// ////////////////////////////////////////////////////////////////////////////
	{private ["_a","_b","_c","_d","_e"];

	_a = _this select 0;
	_b = _this select 1;

	if (count _b > 0) then
	{
		if (count (GL5_Reinforcement select 1) > 0) then
		{
			_b = _b - (GL5_Reinforcement select 1);
		};

		if (count (GL5_Advancing select 0) > 0) then
		{
			_b = _b - (GL5_Advancing select 0);
		};

		if (count (GL5_Suppressed select 0) > 0) then
		{
			_b = _b - (GL5_Suppressed select 0);
		};

		_c = 0;

		while { (_c < count _b) } do
		{
			_d = (_b select _c);

			_e = (vehicle leader _d);

			if (_e isKindOf "CaManBase") then
			{
				[_a, _d] call (GL5_Advancing_F select 2);
			};

			_c = _c + 1;
		};
	};

	},

	// ////////////////////////////////////////////////////////////////////////////
	// Advancing Function #2
	// ////////////////////////////////////////////////////////////////////////////
	// Advancing
	//
	// ////////////////////////////////////////////////////////////////////////////
	{private ["_a","_b","_c"];

	_a = _this select 0;
	_b = _this select 1;

	_c = (_a distance leader _b);

	switch (GL5_Advancing select 2) do
	{
		case 0 :
		{
			if (_c <= (GL5_Global select 38) ) then
			{
				[_a, _b, _c] spawn (GL5_Advancing_F select 3);
			};
		};

		case 1 :
		{
			if (_c <= (GL5_Global select 39) ) then
			{
				[_a, _b, _c] spawn (GL5_Advancing_F select 3);
			};
		};

		case 2 :
		{
			if (_c <= (GL5_Global select 40) ) then
			{
				[_a, _b, _c] spawn (GL5_Advancing_F select 3);
			};
		};

		case 3 :
		{
			if (_c <= (GL5_Global select 41) ) then
			{
				[_a, _b, _c] spawn (GL5_Advancing_F select 3);
			};
		};
	};

	},

	// ////////////////////////////////////////////////////////////////////////////
	// Advancing Function #3
	// ////////////////////////////////////////////////////////////////////////////
	// Advancing
	//
	// ////////////////////////////////////////////////////////////////////////////
	{private ["_a","_b","_c","_d","_e","_f"];

	_a = _this select 0;
	_b = _this select 1;
	_c = _this select 2;

	_d = (vehicle leader _b);

	if (_b in (GL5_Static select 0) ) then
	{
		{if (floor (random 100) < 75) then {_x setUnitPos "DOWN"}; } count units _b;

		if (_c > 100) then
		{
			if (_d hasWeapon "Binocular") then {_d selectWeapon "Binocular"};
		};

		{if (floor (random 100) < 50) then {_x doWatch (getPosATL _a)}; } count units _b;

		//["Advancing", _b] call (GL5_Dubbing_F select 4);

		sleep (random 30);

		{_x doWatch objNull} count units _b;

		if !(_b in (GL5_Suppressed select 0) ) then {{_x setUnitPos "AUTO" } count units _b};
	}
	else
	{
		if ( (floor (random 100) < 50) && (_c > 50) ) then
		{
			GL5_Advancing set [0, (GL5_Advancing select 0) + [_b] ];

			_e = behaviour _d;
			_f = speedMode _d;

			if (floor (random 100) < 50) then
			{
				_b setBehaviour "AWARE";
				_b setSpeedMode "NORMAL";
			}
			else
			{
				_b setBehaviour "COMBAT";
				_b setSpeedMode "FULL";
			};

			{if (floor (random 100) < 50) then {_x doWatch (getPosATL _a)}; } count units _b;

			//["Advancing", _b] call (GL5_Dubbing_F select 4);

			_b lockWp True;

			{if ( (floor (random 100) < 75) || (_x == _d) ) then {_x doMove (getPosATL _a)}; } count units _b;

			sleep (random 50);

			{_x doWatch objNull} count units _b;

			_b setBehaviour _e;
			_b setSpeedMode _f;

			GL5_Advancing set [0, (GL5_Advancing select 0) - [_b] ];

			if !(_b in (GL5_Reinforcement select 1) ) then {{ doStop _x; } count units _b};
		}
		else
		{
			{if (floor (random 100) < 75) then {_x setUnitPos "DOWN"}; } count units _b;

			if (_c > 100) then
			{
				if (_d hasWeapon "Binocular") then {_d selectWeapon "Binocular"};
			};

			{if (floor (random 100) < 50) then {_x doWatch (getPosATL _a)}; } count units _b;

			//["Advancing", _b] call (GL5_Dubbing_F select 4);

			sleep (random 30);

			{_x doWatch objNull} count units _b;

			if !(_b in (GL5_Suppressed select 0) ) then {{_x setUnitPos "AUTO"} count units _b};
		};
	};

	},

	// ////////////////////////////////////////////////////////////////////////////
	// Advancing Function #4
	// ////////////////////////////////////////////////////////////////////////////
	// Advancing
	//
	// ////////////////////////////////////////////////////////////////////////////
	{private ["_a","_b","_c","_d","_e","_f","_g"];

	_a = _this select 0;
	_b = _this select 1;

	while { (getPosATL _b select 2 > 1) } do
	{
		sleep 0.1;
	};

	_c = (GL5_Groups select 0);

	if (count _c > 0) then
	{
		_d = 0;

		while { (_d < count _c) } do
		{
			_e = (_c select _d);

			_f = (vehicle leader _e);

			if ( (alive _f) && { (_f isKindOf "CaManBase") } && { (_f distance _b < (GL5_Global select 34) ) } ) exitWith
			{
				sleep 1 + (random 1);

				_g = (_f distance _b);

				[_a, _e, _g] spawn (GL5_Advancing_F select 3);
			};

			if (getPosATL _b select 2 > 1) then
			{
				[_a, _b] spawn (GL5_Advancing_F select 4);
			};

			_d = _d + 1;
		};
	};

	}
];