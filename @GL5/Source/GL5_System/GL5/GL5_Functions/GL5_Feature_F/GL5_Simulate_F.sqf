// ////////////////////////////////////////////////////////////////////////////
// GL5 v.1.0
// ////////////////////////////////////////////////////////////////////////////
// Simulate Functions
//
// ////////////////////////////////////////////////////////////////////////////

GL5_Simulate_F = [

	// ////////////////////////////////////////////////////////////////////////////
	// Simulate Function #0
	// ////////////////////////////////////////////////////////////////////////////
	// Simulate
	//
	// ////////////////////////////////////////////////////////////////////////////
	{private ["_a","_b","_c"];

	_a = _this select 0;
	_b = _this select 1;

	if ( (_a countEnemy [_b] > 0) && { ( [_a] call (GL5_Simulate_F select 1) ) } && { !(_a in (GL5_Simulate select 0) ) } ) then
	{
		GL5_Simulate set [0, (GL5_Simulate select 0) + [_a] ];

		hint "Warning! There are no allied nearby. You should think about simulating dead.";

		_c = _a addAction ["Simulate Dead", (GL5_Path+"GL5\GL5_Features\GL5_Simulate_Dead\GL5_Simulate_Dead.sqf")];

		sleep 10;

		if (alive _a) then
		{
			if (animationState _a != "Unconscious") then
			{
				_a removeAction _c;

				hint "";

				sleep 50 + (random 100);

				GL5_Simulate set [0, (GL5_Simulate select 0) - [_a] ];
			};
		}
		else
		{
			_a removeAction _c;

			hint "";

			GL5_Simulate set [0, (GL5_Simulate select 0) - [_a] ];
		};
	};

	},

	// ////////////////////////////////////////////////////////////////////////////
	// Simulate Function #1
	// ////////////////////////////////////////////////////////////////////////////
	// Simulate
	//
	// ////////////////////////////////////////////////////////////////////////////
	{private ["_a","_b","_c","_d"];

	_a = _this select 0;

	_b = True;

	_c = 0;

	while { ( (_c < count units _a) && (_b) ) } do
	{
		_d = (units _a select _c);

		if ( (_d != _a) && (alive _d) ) then
		{
			if (_d distance _a > (GL5_Local select 85) ) then
			{
				_b = True;
			}
			else
			{
				_b = False;
			};
		};

		_c = _c + 1;
	};

	_b

	},

	// ////////////////////////////////////////////////////////////////////////////
	// Simulate Function #2
	// ////////////////////////////////////////////////////////////////////////////
	// Simulate
	//
	// ////////////////////////////////////////////////////////////////////////////
	{private ["_a","_b","_c"];

	_a = _this select 0;
	_b = _this select 1;

	if ( (alive _a) && { (_a countEnemy [_b] > 0) } && { ( [_a] call (GL5_Simulate_F select 3) ) } ) then
	{
		if ( { (animationState _a == _x) } count (GL5_Resource select 37) > 0) then
		{
			_a switchMove "AinjPpneMstpSnonWnonDnon_kneel";
		}
		else
		{
			if (stopped _a) then
			{
				_a stop False;
			};

			_c = (GL5_Resource select 37) call GL5_Random_Select_F;

			_a playAction _c;

			sleep 30 + (random 50);

			if (alive _a ) then
			{
				_a switchMove "AinjPpneMstpSnonWnonDnon_kneel";
			};
		};
	};

	},

	// ////////////////////////////////////////////////////////////////////////////
	// Simulate Function #3
	// ////////////////////////////////////////////////////////////////////////////
	// Simulate
	//
	// ////////////////////////////////////////////////////////////////////////////
	{private ["_a","_b","_c","_d","_e","_f"];

	_a = _this select 0;

	_b = True;

	_c = (_a) nearEntities [["CaManBase","Car","Tank","Air"], 100];

	_c = _c - [_a];

	if (count _c > 0) then
	{
		_d = [];

		_e = 0;

		while { (_e < count _c) } do
		{
			_f = (_c select _e);

			if ( (alive _f) && { (_a countFriendly [_f] > 0) } && { (side _f != CIVILIAN) } && { (_a knowsAbout _f > 0) } && { (count weapons _f > 0) } && { (count magazines _f > 0) } ) then
			{
				_d = _d + [_f];
			};

			if (count _d > (GL5_Global select 60) ) exitWith
			{
				_b = False;
			};

			_e = _e + 1;
		};
	};

	_b

	}
];		