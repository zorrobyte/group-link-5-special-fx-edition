// ////////////////////////////////////////////////////////////////////////////
// GL5 v.1.0 System Config
//
// ////////////////////////////////////////////////////////////////////////////

class CfgPatches
{
	class GL5_System
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 1.0;
		requiredAddons[] = {"A3_Data_F","A3_Modules_F","A3_UI_F"};
		author[] = {"Zorrobyte"};
		versionDesc = "GroupLink_5_Core";
		version = "1.0";
	};
};

#include "CfgResource.hpp"

#define GL5_EH_System "if (isNil ""GL5_Path"") then {GL5_Path = ""\GL5_System\""}; _this execVM (GL5_Path+""GL5_System.sqf"")";

#define GL5_EH_Defend "if (isNil ""GL5_Path"") then {GL5_Path = ""\GL5_System\""}; if (isServer) then {_this execVM (GL5_Path+""GL5\GL5_System\GL5_Defend.sqf"") }";

#define GL5_EH_Static "if (isNil ""GL5_Path"") then {GL5_Path = ""\GL5_System\""}; if (isServer) then {_this execVM (GL5_Path+""GL5\GL5_System\GL5_Static.sqf"") }";

#define GL5_EH_Custom "if (isNil ""GL5_Path"") then {GL5_Path = ""\GL5_System\""}; if (isServer) then {_this execVM (GL5_Path+""GL5\GL5_System\GL5_Custom.sqf"") }";

#define GL5_EH_Special_FX "if (isNil ""GL5_Path"") then {GL5_Path = ""\GL5_System\""}; _this execVM (GL5_Path+""GL5\GL5_System\GL5_Special_FX.sqf"")";

#define GL5_EH_Time "if (isNil ""GL5_Path"") then {GL5_Path = ""\GL5_System\""}; _this execVM (GL5_Path+""GL5\GL5_System\GL5_Random\GL5_Time.sqf"")";

#define GL5_EH_Weather "if (isNil ""GL5_Path"") then {GL5_Path = ""\GL5_System\""}; _this execVM (GL5_Path+""GL5\GL5_System\GL5_Random\GL5_Weather.sqf"")";

#define GL5_EH_Position "if (isNil ""GL5_Path"") then {GL5_Path = ""\GL5_System\""}; _this execVM (GL5_Path+""GL5\GL5_System\GL5_Random\GL5_Position.sqf"")";

#define GL5_EH_Snow "if (isNil ""GL5_Path"") then {GL5_Path = ""\GL5_System\""}; _this execVM (GL5_Path+""GL5\GL5_System\GL5_Random\GL5_Snow.sqf"")";

#define GL5_EH_HC_Reinforcement "if (isNil ""GL5_Path"") then {GL5_Path = ""\GL5_System\""}; _this execVM (GL5_Path+""GL5\GL5_System\GL5_HC_Reinforcement.sqf"")";

#define GL5_EH_HC_Helicopter "if (isNil ""GL5_Path"") then {GL5_Path = ""\GL5_System\""}; _this execVM (GL5_Path+""GL5\GL5_System\GL5_HC_Helicopter.sqf"")";

#define GL5_EH_HC_Artillery "if (isNil ""GL5_Path"") then {GL5_Path = ""\GL5_System\""}; _this execVM (GL5_Path+""GL5\GL5_System\GL5_HC_Artillery.sqf"")";

class Extended_Init_Eventhandlers
{
	class CAManBase
	{
		class GL5_System
		{
			init = GL5_EH_System
		};
	};
};

class CfgVehicles
{
	class Logic ;

	class GL5_System : Logic
	{
		displayName = "Group Link 5: System [ Initialize ]";
		icon = "iconModule";
		picture = "\A3\modules_f\data\iconmodule_ca.paa";
		vehicleClass = "Modules";

		class EventHandlers
		{
			init = GL5_EH_System
		};
	};

	class GL5_Defend : Logic
	{
		displayName = "Group Link 5: A.I. Enhancement [ Defend ]";
		icon = "iconModule";
		picture = "\A3\modules_f\data\iconmodule_ca.paa";
		vehicleClass = "Modules";

		class EventHandlers
		{
			init = GL5_EH_Defend
		};
	};

	class GL5_Static : Logic
	{
		displayName = "Group Link 5: A.I. Enhancement [ Static ]";
		icon = "iconModule";
		picture = "\A3\modules_f\data\iconmodule_ca.paa";
		vehicleClass = "Modules";

		class EventHandlers
		{
			init = GL5_EH_Static
		};
	};

	class GL5_Custom : Logic
	{
		displayName = "Group Link 5: A.I. Enhancement [ Custom ]";
		icon = "iconModule";
		picture = "\A3\modules_f\data\iconmodule_ca.paa";
		vehicleClass = "Modules";

		class EventHandlers
		{
			init = GL5_EH_Custom
		};
	};

	class GL5_Special_FX : Logic
	{
		displayName = "Group Link 5: Special FX [ Initialize ]";
		icon = "iconModule";
		picture = "\A3\modules_f\data\iconmodule_ca.paa";
		vehicleClass = "Modules";

		class EventHandlers
		{
			init = GL5_EH_Special_FX
		};
	};

	class GL5_Time : Logic
	{
		displayName = "Group Link 5: Random Time";
		icon = "iconModule";
		picture = "\A3\modules_f\data\iconmodule_ca.paa";
		vehicleClass = "Modules";

		class EventHandlers
		{
			init = GL5_EH_Time
		};
	};

	class GL5_Weather : Logic
	{
		displayName = "Group Link 5: Random Weather";
		icon = "iconModule";
		picture = "\A3\modules_f\data\iconmodule_ca.paa";
		vehicleClass = "Modules";

		class EventHandlers
		{
			init = GL5_EH_Weather
		};
	};

	class GL5_Position : Logic
	{
		displayName = "Group Link 5: Random Position";
		icon = "iconModule";
		picture = "\A3\modules_f\data\iconmodule_ca.paa";
		vehicleClass = "Modules";

		class EventHandlers
		{
			init = GL5_EH_Position
		};
	};

	class GL5_Snow : Logic
	{
		displayName = "Group Link 5: Random Snow";
		icon = "iconModule";
		picture = "\A3\modules_f\data\iconmodule_ca.paa";
		vehicleClass = "Modules";

		class EventHandlers
		{
			init = GL5_EH_Snow
		};
	};

	class GL5_HC_Reinforcement : Logic
	{
		displayName = "Group Link 5: High Command Reinforcement";
		icon = "iconModule";
		picture = "\A3\modules_f\data\iconmodule_ca.paa";
		vehicleClass = "Modules";

		class EventHandlers
		{
			init = GL5_EH_HC_Reinforcement
		};
	};

	class GL5_HC_Helicopter : Logic
	{
		displayName = "Group Link 5: High Command Helicopter";
		icon = "iconModule";
		picture = "\A3\modules_f\data\iconmodule_ca.paa";
		vehicleClass = "Modules";

		class EventHandlers
		{
			init = GL5_EH_HC_Helicopter
		};
	};

	class GL5_HC_Artillery : Logic
	{
		displayName = "Group Link 5: High Command Artillery";
		icon = "iconModule";
		picture = "\A3\modules_f\data\iconmodule_ca.paa";
		vehicleClass = "Modules";

		class EventHandlers
		{
			init = GL5_EH_HC_Artillery
		};
	};
};

class CfgAddOns
{
	class PreloadBanks {};
	
	class PreloadAddOns
	{
		class GL5_System
		{
			list[] = {"GL5_System"};
		};
	};
};