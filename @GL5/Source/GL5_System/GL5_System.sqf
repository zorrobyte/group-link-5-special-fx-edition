// ////////////////////////////////////////////////////////////////////////////
// GL5 v.1.0
// ////////////////////////////////////////////////////////////////////////////
// System Initialize
//
// ////////////////////////////////////////////////////////////////////////////
private ["_a","_b","_c","_d","_e"];

_a = _this select 0;

if (isNil "GL5_Initialize") then
{
	GL5_Initialize = [ False, False, False ];

	GL5_Synchronize = [ False, False, False ];

	GL5_Default = [ False, False, False ];

	if (isServer) then
	{
		call compile preprocessFile (GL5_Path+"GL5\GL5_System\GL5_Preprocess.sqf");
	}
	else
	{
		call compile preprocessFile (GL5_Path+"GL5\GL5_System\GL5_Preprocess.sqf");

		if (_a isKindOf "CaManBase") then
		{
			GL5_System_PublicVariable = [1]; publicVariable "GL5_System_PublicVariable";

			_b = time + 1;

			waitUntil { (time > _b) };
		};
	};
};

if ( (_a isKindOf "CaManBase") && { (typeName (GL5_Default select 0) == "Bool") } && { (GL5_Core select 0) } ) exitWith
{
	_b = time + 1;

	waitUntil { (time > _b) };

	if (typeName (GL5_Default select 0) == "Bool") then
	{
		GL5_Default set [0, _a];

		["Default", 1] execVM (GL5_Path+"GL5\GL5_System\GL5_Display.sqf");

		if (isServer) then
		{
			_c = allUnits;

			{if ( { (isPlayer _x) } count (units _x) > 0) exitWith {_d = (vehicle _x) } } forEach _c;

			if (typeName _d == "Object") then
			{
				_e = [_c, _d] call compile preprocessFile (GL5_Path+"GL5\GL5_System\GL5_System.sqf");

				[ (_e select 0), (_e select 1) ] call compile preprocessFile (GL5_Path+"GL5\GL5_System\GL5_Public.sqf");
			};
		}
		else
		{
			call compile preprocessFile (GL5_Path+"GL5\GL5_System\GL5_Public.sqf");
		};

		if (GL5_Core select 3) then
		{
			_this execVM (GL5_Path+"GL5\GL5_System\GL5_Special_FX.sqf");
		};

		GL5_Initialize set [3, True];

		["Default", 2] execVM (GL5_Path+"GL5\GL5_System\GL5_Display.sqf");
	};
};

if ( (_a isKindOf "Logic") && (typeName (GL5_Default select 0) == "Bool") ) exitWith
{
	GL5_Default set [0, _a];

	_b = time + 1;

	waitUntil { (time > _b) };

	["Module", 1] execVM (GL5_Path+"GL5\GL5_System\GL5_Display.sqf");

	if (isServer) then
	{
		_c = allUnits;

		{if ( { (isPlayer _x) } count (units _x) > 0) exitWith {_d = (vehicle _x) } } forEach _c;

		_c = (synchronizedObjects _a);

		_c = _c - [_a];

		if (typeName _d == "Object") then
		{
			_e = [_c, _d] call compile preprocessFile (GL5_Path+"GL5\GL5_System\GL5_System.sqf");

			[ (_e select 0), (_e select 1) ] call compile preprocessFile (GL5_Path+"GL5\GL5_System\GL5_Public.sqf");
		};
	}
	else
	{
		call compile preprocessFile (GL5_Path+"GL5\GL5_System\GL5_Public.sqf");
	};

	GL5_Initialize set [3, True];

	["Module", 2] execVM (GL5_Path+"GL5\GL5_System\GL5_Display.sqf");
};