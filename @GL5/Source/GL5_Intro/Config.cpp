// ////////////////////////////////////////////////////////////////////////////
// GL5 v.1.0 Intro Config
// 
// ////////////////////////////////////////////////////////////////////////////

class CfgPatches
{
	class GL5_Intro
	{
		units[] = {"Utes"};
		weapons[] = {};
		requiredVersion = 1.0;
		requiredAddons[] = {"Utes"};
	};
};

class CfgWorlds
{
	class CAWorld;

	class utes : CAWorld
	{
		cutscenes[] = {"GL5_Intro_Church", "GL5_Intro_AH64", "GL5_Intro_Tower"};
	};
};

class CfgMissions
{
	class Cutscenes
	{
		class GL5_Intro_Church
		{
			directory = "GL5_Intro\GL5_Intro_Church.utes";
		};

		class GL5_Intro_AH64
		{
			directory = "GL5_Intro\GL5_Intro_AH64.utes";
		};

		class GL5_Intro_Tower
		{
			directory = "GL5_Intro\GL5_Intro_Tower.utes";
		};
	};
};