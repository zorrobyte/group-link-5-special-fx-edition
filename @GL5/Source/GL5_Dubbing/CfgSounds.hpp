// ////////////////////////////////////////////////////////////////////////////
// GL5 v.1.0 Dubbing Config
// 
// ////////////////////////////////////////////////////////////////////////////

class CfgSounds
{
	// //////////////////////////////////////
	// Dubbing West Safe
	// //////////////////////////////////////

	class West_Safe_v01
	{
		name = "West_Safe_v01";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\ALICE\Generic\Laugh01.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Safe_v02
	{
		name = "West_Safe_v02";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\ALICE\Generic\Laugh02.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Safe_v03
	{
		name = "West_Safe_v03";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\ALICE\Generic\Laugh03.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Safe_v04
	{
		name = "West_Safe_v04";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\ALICE\Generic\Laugh04.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Safe_v05
	{
		name = "West_Safe_v05";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\ALICE\Generic\Sigh01.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Safe_v06
	{
		name = "West_Safe_v06";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\ALICE\Generic\Sigh02.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Safe_v07
	{
		name = "West_Safe_v07";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\ALICE\Generic\Sigh03.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Safe_v08
	{
		name = "West_Safe_v08";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\ALICE\Generic\Sigh04.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Safe_v09
	{
		name = "West_Safe_v09";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\ALICE\Generic\Weep01.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Safe_v10
	{
		name = "West_Safe_v10";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\ALICE\Generic\Weep02.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Safe_v11
	{
		name = "West_Safe_v11";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\ALICE\Generic\Weep03.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Safe_v12
	{
		name = "West_Safe_v12";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\ALICE\Generic\Weep04.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Safe_v13
	{
		name = "West_Safe_v13";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\ALICE\Generic\Whistle01.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Safe_v14
	{
		name = "West_Safe_v14";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\ALICE\Generic\Whistle02.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Safe_v15
	{
		name = "West_Safe_v15";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\ALICE\Generic\Yawn01.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Safe_v16
	{
		name = "West_Safe_v16";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\ALICE\Generic\Yawn02.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Safe_v17
	{
		name = "West_Safe_v17";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\ALICE\Generic\Laugh01.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Safe_v18
	{
		name = "West_Safe_v18";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\ALICE\Generic\Laugh02.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Safe_v19
	{
		name = "West_Safe_v19";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\ALICE\Generic\Laugh03.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Safe_v20
	{
		name = "West_Safe_v20";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\ALICE\Generic\Laugh04.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Safe_v21
	{
		name = "West_Safe_v21";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\ALICE\Generic\Sigh01.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Safe_v22
	{
		name = "West_Safe_v22";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\ALICE\Generic\Sigh02.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Safe_v23
	{
		name = "West_Safe_v23";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\ALICE\Generic\Sigh03.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Safe_v24
	{
		name = "West_Safe_v24";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\ALICE\Generic\Sigh04.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Safe_v25
	{
		name = "West_Safe_v25";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\ALICE\Generic\Weep01.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Safe_v26
	{
		name = "West_Safe_v26";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\ALICE\Generic\Weep02.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Safe_v27
	{
		name = "West_Safe_v27";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\ALICE\Generic\Weep03.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Safe_v28
	{
		name = "West_Safe_v28";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\ALICE\Generic\Weep04.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Safe_v29
	{
		name = "West_Safe_v29";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\ALICE\Generic\Whistle01.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Safe_v30
	{
		name = "West_Safe_v30";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\ALICE\Generic\Whistle02.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Safe_v31
	{
		name = "West_Safe_v31";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\ALICE\Generic\Yawn01.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Safe_v32
	{
		name = "West_Safe_v32";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\ALICE\Generic\Yawn02.wss", db-5, 1, 200};
		titles[] = {};
	};


	// //////////////////////////////////////
	// Dubbing East Safe
	// //////////////////////////////////////

	class East_Safe_v01
	{
		name = "East_Safe_v01";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\ALICE\Generic\Generic_01.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Safe_v02
	{
		name = "East_Safe_v02";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\ALICE\Generic\Generic_02.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Safe_v03
	{
		name = "East_Safe_v03";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\ALICE\Generic\Generic_03.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Safe_v04
	{
		name = "East_Safe_v04";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\ALICE\Generic\Generic_04.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Safe_v05
	{
		name = "East_Safe_v05";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\ALICE\Generic\Generic_05.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Safe_v06
	{
		name = "East_Safe_v06";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\ALICE\Generic\Generic_06.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Safe_v07
	{
		name = "East_Safe_v07";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\ALICE\Generic\Generic_07.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Safe_v08
	{
		name = "East_Safe_v08";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\ALICE\Generic\Generic_08.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Safe_v09
	{
		name = "East_Safe_v09";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\ALICE\Generic\Laugh01.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Safe_v10
	{
		name = "East_Safe_v10";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\ALICE\Generic\Laugh02.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Safe_v11
	{
		name = "East_Safe_v11";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\ALICE\Generic\Laugh03.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Safe_v12
	{
		name = "East_Safe_v12";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\ALICE\Generic\Laugh04.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Safe_v13
	{
		name = "East_Safe_v13";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\ALICE\Generic\Sigh01.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Safe_v14
	{
		name = "East_Safe_v14";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\ALICE\Generic\Sigh02.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Safe_v15
	{
		name = "East_Safe_v15";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\ALICE\Generic\Sigh03.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Safe_v16
	{
		name = "East_Safe_v16";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\ALICE\Generic\Sigh04.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Safe_v17
	{
		name = "East_Safe_v17";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\ALICE\Generic\Weep01.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Safe_v18
	{
		name = "East_Safe_v18";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\ALICE\Generic\Weep02.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Safe_v19
	{
		name = "East_Safe_v19";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\ALICE\Generic\Weep03.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Safe_v20
	{
		name = "East_Safe_v20";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\ALICE\Generic\Weep04.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Safe_v21
	{
		name = "East_Safe_v21";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\ALICE\Generic\Whistle01.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Safe_v22
	{
		name = "East_Safe_v22";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\ALICE\Generic\Whistle02.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Safe_v23
	{
		name = "East_Safe_v23";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\ALICE\Generic\Yawn01.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Safe_v24
	{
		name = "East_Safe_v24";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\ALICE\Generic\Yawn02.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Safe_v25
	{
		name = "East_Safe_v25";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\ALICE\Generic\Generic_01.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Safe_v26
	{
		name = "East_Safe_v26";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\ALICE\Generic\Generic_02.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Safe_v27
	{
		name = "East_Safe_v27";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\ALICE\Generic\Generic_03.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Safe_v28
	{
		name = "East_Safe_v28";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\ALICE\Generic\Generic_04.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Safe_v29
	{
		name = "East_Safe_v29";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\ALICE\Generic\Generic_05.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Safe_v30
	{
		name = "East_Safe_v30";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\ALICE\Generic\Generic_06.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Safe_v31
	{
		name = "East_Safe_v31";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\ALICE\Generic\Generic_07.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Safe_v32
	{
		name = "East_Safe_v32";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\ALICE\Generic\Generic_08.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Safe_v33
	{
		name = "East_Safe_v33";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\ALICE\Generic\Laugh01.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Safe_v34
	{
		name = "East_Safe_v34";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\ALICE\Generic\Laugh02.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Safe_v35
	{
		name = "East_Safe_v35";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\ALICE\Generic\Laugh03.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Safe_v36
	{
		name = "East_Safe_v36";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\ALICE\Generic\Laugh04.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Safe_v37
	{
		name = "East_Safe_v37";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\ALICE\Generic\Sigh01.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Safe_v38
	{
		name = "East_Safe_v38";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\ALICE\Generic\Sigh02.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Safe_v39
	{
		name = "East_Safe_v39";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\ALICE\Generic\Sigh03.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Safe_v40
	{
		name = "East_Safe_v40";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\ALICE\Generic\Sigh04.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Safe_v41
	{
		name = "East_Safe_v41";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\ALICE\Generic\Weep01.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Safe_v42
	{
		name = "East_Safe_v42";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\ALICE\Generic\Weep02.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Safe_v43
	{
		name = "East_Safe_v43";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\ALICE\Generic\Weep03.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Safe_v44
	{
		name = "East_Safe_v44";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\ALICE\Generic\Weep04.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Safe_v45
	{
		name = "East_Safe_v45";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\ALICE\Generic\Whistle01.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Safe_v46
	{
		name = "East_Safe_v46";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\ALICE\Generic\Whistle02.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Safe_v47
	{
		name = "East_Safe_v47";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\ALICE\Generic\Yawn01.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Safe_v48
	{
		name = "East_Safe_v48";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\ALICE\Generic\Yawn02.wss", db-5, 1, 200};
		titles[] = {};
	};

	// //////////////////////////////////////
	// Dubbing West Aware ( No Contact )
	// //////////////////////////////////////

	class West_Aware_v01
	{
		name = "West_Aware_v01";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\combat\CarefulJustBeCareful.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v02
	{
		name = "West_Aware_v02";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\combat\CheckSectors.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v03
	{
		name = "West_Aware_v03";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\combat\GoILLCoverE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v04
	{
		name = "West_Aware_v04";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\combat\GoImCoveringE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v05
	{
		name = "West_Aware_v05";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\combat\OKLetsGo.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v06
	{
		name = "West_Aware_v06";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\combat\OpenUpE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v07
	{
		name = "West_Aware_v07";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\combat\ScanYourSector.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v08
	{
		name = "West_Aware_v08";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\AwaitingOrders.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v09
	{
		name = "West_Aware_v09";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\BeAdvised.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v10
	{
		name = "West_Aware_v10";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\CoverAllSectors.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v11
	{
		name = "West_Aware_v11";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\CoverOurRear.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v12
	{
		name = "West_Aware_v12";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\GetReadyForContactE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v13
	{
		name = "West_Aware_v13";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\GetReadyToFightE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v14
	{
		name = "West_Aware_v14";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\ReadyForOrders.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v15
	{
		name = "West_Aware_v15";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\ReadyToFire.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v16
	{
		name = "West_Aware_v16";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\ScanHorizon.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v17
	{
		name = "West_Aware_v17";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\StayAlert.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v18
	{
		name = "West_Aware_v18";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\TakePoint.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v19
	{
		name = "West_Aware_v19";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\TakePoint.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v20
	{
		name = "West_Aware_v10";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\WatchThatPosition.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v21
	{
		name = "West_Aware_v21";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\combat\CarefulJustBeCareful.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v22
	{
		name = "West_Aware_v22";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\combat\CheckSectors.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v23
	{
		name = "West_Aware_v23";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\combat\GoILLCoverE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v24
	{
		name = "West_Aware_v24";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\combat\GoImCoveringE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v25
	{
		name = "West_Aware_v25";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\combat\OKLetsGo.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v26
	{
		name = "West_Aware_v26";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\combat\OpenUpE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v27
	{
		name = "West_Aware_v27";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\combat\ScanYourSector.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v28
	{
		name = "West_Aware_v28";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\AwaitingOrders.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v29
	{
		name = "West_Aware_v29";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\BeAdvised.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v30
	{
		name = "West_Aware_v30";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\CoverAllSectors.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v31
	{
		name = "West_Aware_v31";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\CoverOurRear.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v32
	{
		name = "West_Aware_v32";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\GetReadyForContactE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v33
	{
		name = "West_Aware_v33";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\GetReadyToFightE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v34
	{
		name = "West_Aware_v34";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\ReadyForOrders.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v35
	{
		name = "West_Aware_v35";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\ReadyToFire.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v36
	{
		name = "West_Aware_v36";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\ScanHorizon.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v37
	{
		name = "West_Aware_v37";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\StayAlert.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v38
	{
		name = "West_Aware_v38";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\TakePoint.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v39
	{
		name = "West_Aware_v39";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\TakePoint.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v40
	{
		name = "West_Aware_v40";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\WatchThatPosition.wss", db-5, 1, 200};
		titles[] = {};
	};

	// //////////////////////////////////////
	// Dubbing West Aware ( Contact )
	// //////////////////////////////////////

	class West_Aware_v41
	{
		name = "West_Aware_v41";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\ContactE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v42
	{
		name = "West_Aware_v42";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\DamnE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v43
	{
		name = "West_Aware_v43";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\DangerE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v44
	{
		name = "West_Aware_v44";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\DownAndQuiet.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v45
	{
		name = "West_Aware_v45";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\EnemiesE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v46
	{
		name = "West_Aware_v46";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\EngageAtWill.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v47
	{
		name = "West_Aware_v47";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\EngageE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v48
	{
		name = "West_Aware_v48";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\EngagingE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v49
	{
		name = "West_Aware_v49";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\EngagingTargetE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v50
	{
		name = "West_Aware_v50";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\EyesOnTarget.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v51
	{
		name = "West_Aware_v51";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\FireAtWill.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v52
	{
		name = "West_Aware_v52";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\FireE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v53
	{
		name = "West_Aware_v53";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\FuckE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v54
	{
		name = "West_Aware_v54";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\FuckThatsCloseE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v55
	{
		name = "West_Aware_v55";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\GetDown.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v56
	{
		name = "West_Aware_v56";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\GiveEmHellE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v57
	{
		name = "West_Aware_v57";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\GoddamnE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v58
	{
		name = "West_Aware_v58";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\GoGoGoE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v59
	{
		name = "West_Aware_v59";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\HostilesE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v60
	{
		name = "West_Aware_v60";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\LightEmUpE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v61
	{
		name = "West_Aware_v61";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\ContactE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v62
	{
		name = "West_Aware_v62";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\DamnE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v63
	{
		name = "West_Aware_v63";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\DangerE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v64
	{
		name = "West_Aware_v64";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\DownAndQuiet.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v65
	{
		name = "West_Aware_v65";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\EnemiesE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v66
	{
		name = "West_Aware_v66";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\EngageAtWill.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v67
	{
		name = "West_Aware_v67";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\EngageE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v68
	{
		name = "West_Aware_v68";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\EngagingE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v69
	{
		name = "West_Aware_v69";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\EngagingTargetE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v70
	{
		name = "West_Aware_v70";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\EyesOnTarget.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v71
	{
		name = "West_Aware_v71";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\FireAtWill.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v72
	{
		name = "West_Aware_v72";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\FireE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v73
	{
		name = "West_Aware_v73";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\FuckE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v74
	{
		name = "West_Aware_v74";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\FuckThatsCloseE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v75
	{
		name = "West_Aware_v75";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\GetDown.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v76
	{
		name = "West_Aware_v76";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\GiveEmHellE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v77
	{
		name = "West_Aware_v77";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\GoddamnE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v78
	{
		name = "West_Aware_v78";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\GoGoGoE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v79
	{
		name = "West_Aware_v79";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\HostilesE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Aware_v80
	{
		name = "West_Aware_v80";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\LightEmUpE.wss", db-5, 1, 200};
		titles[] = {};
	};

	// //////////////////////////////////////
	// Dubbing East Aware ( No Contact )
	// //////////////////////////////////////

	class East_Aware_v01
	{
		name = "East_Aware_v01";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\combat\CarefulJustBeCareful.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v02
	{
		name = "East_Aware_v02";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\combat\CheckSectors.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v03
	{
		name = "East_Aware_v03";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\combat\GoILLCoverE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v04
	{
		name = "East_Aware_v04";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\combat\GoImCoveringE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v05
	{
		name = "East_Aware_v05";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\combat\OKLetsGo.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v06
	{
		name = "East_Aware_v06";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\combat\OpenUpE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v07
	{
		name = "East_Aware_v07";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\combat\ScanYourSector.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v08
	{
		name = "East_Aware_v08";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\AwaitingOrders.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v09
	{
		name = "East_Aware_v09";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\BeAdvised.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v10
	{
		name = "East_Aware_v10";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\CoverAllSectors.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v11
	{
		name = "East_Aware_v11";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\CoverOurRear.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v12
	{
		name = "East_Aware_v12";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\GetReadyForContactE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v13
	{
		name = "East_Aware_v13";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\GetReadyToFightE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v14
	{
		name = "East_Aware_v14";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\ReadyForOrders.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v15
	{
		name = "East_Aware_v15";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\ReadyToFire.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v16
	{
		name = "East_Aware_v16";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\ScanHorizon.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v17
	{
		name = "East_Aware_v17";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\StayAlert.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v18
	{
		name = "East_Aware_v18";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\TakePoint.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v19
	{
		name = "East_Aware_v19";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\TakePoint.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v20
	{
		name = "East_Aware_v10";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\WatchThatPosition.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v21
	{
		name = "East_Aware_v21";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\combat\CarefulJustBeCareful.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v22
	{
		name = "East_Aware_v22";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\combat\CheckSectors.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v23
	{
		name = "East_Aware_v23";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\combat\GoILLCoverE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v24
	{
		name = "East_Aware_v24";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\combat\GoImCoveringE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v25
	{
		name = "East_Aware_v25";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\combat\OKLetsGo.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v26
	{
		name = "East_Aware_v26";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\combat\OpenUpE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v27
	{
		name = "East_Aware_v27";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\combat\ScanYourSector.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v28
	{
		name = "East_Aware_v28";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\AwaitingOrders.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v29
	{
		name = "East_Aware_v29";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\BeAdvised.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v30
	{
		name = "East_Aware_v30";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\CoverAllSectors.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v31
	{
		name = "East_Aware_v31";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\CoverOurRear.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v32
	{
		name = "East_Aware_v32";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\GetReadyForContactE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v33
	{
		name = "East_Aware_v33";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\GetReadyToFightE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v34
	{
		name = "East_Aware_v34";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\ReadyForOrders.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v35
	{
		name = "East_Aware_v35";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\ReadyToFire.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v36
	{
		name = "East_Aware_v36";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\ScanHorizon.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v37
	{
		name = "East_Aware_v37";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\StayAlert.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v38
	{
		name = "East_Aware_v38";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\TakePoint.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v39
	{
		name = "East_Aware_v39";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\TakePoint.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v40
	{
		name = "East_Aware_v40";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\WatchThatPosition.wss", db-5, 1, 200};
		titles[] = {};
	};

	// //////////////////////////////////////
	// Dubbing East Aware ( Contact )
	// //////////////////////////////////////

	class East_Aware_v41
	{
		name = "East_Aware_v41";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\ContactE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v42
	{
		name = "East_Aware_v42";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\DamnE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v43
	{
		name = "East_Aware_v43";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\DangerE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v44
	{
		name = "East_Aware_v44";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\DownAndQuiet.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v45
	{
		name = "East_Aware_v45";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\EnemiesE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v46
	{
		name = "East_Aware_v46";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\EngageAtWill.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v47
	{
		name = "East_Aware_v47";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\EngageE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v48
	{
		name = "East_Aware_v48";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\EngagingE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v49
	{
		name = "East_Aware_v49";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\EngagingTargetE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v50
	{
		name = "East_Aware_v50";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\EyesOnTarget.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v51
	{
		name = "East_Aware_v51";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\FireAtWill.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v52
	{
		name = "East_Aware_v52";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\FireE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v53
	{
		name = "East_Aware_v53";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\FuckE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v54
	{
		name = "East_Aware_v54";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\FuckThatsCloseE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v55
	{
		name = "East_Aware_v55";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\GetDown.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v56
	{
		name = "East_Aware_v56";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\GiveEmHellE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v57
	{
		name = "East_Aware_v57";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\GoddamnE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v58
	{
		name = "East_Aware_v58";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\GoGoGoE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v59
	{
		name = "East_Aware_v59";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\HostilesE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v60
	{
		name = "East_Aware_v60";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\LightEmUpE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v61
	{
		name = "East_Aware_v61";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\ContactE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v62
	{
		name = "East_Aware_v62";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\DamnE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v63
	{
		name = "East_Aware_v63";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\DangerE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v64
	{
		name = "East_Aware_v64";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\DownAndQuiet.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v65
	{
		name = "East_Aware_v65";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\EnemiesE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v66
	{
		name = "East_Aware_v66";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\EngageAtWill.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v67
	{
		name = "East_Aware_v67";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\EngageE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v68
	{
		name = "East_Aware_v68";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\EngagingE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v69
	{
		name = "East_Aware_v69";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\EngagingTargetE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v70
	{
		name = "East_Aware_v70";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\EyesOnTarget.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v71
	{
		name = "East_Aware_v71";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\FireAtWill.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v72
	{
		name = "East_Aware_v72";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\FireE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v73
	{
		name = "East_Aware_v73";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\FuckE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v74
	{
		name = "East_Aware_v74";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\FuckThatsCloseE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v75
	{
		name = "East_Aware_v75";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\GetDown.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v76
	{
		name = "East_Aware_v76";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\GiveEmHellE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v77
	{
		name = "East_Aware_v77";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\GoddamnE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v78
	{
		name = "East_Aware_v78";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\GoGoGoE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v79
	{
		name = "East_Aware_v79";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\HostilesE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Aware_v80
	{
		name = "East_Aware_v80";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\LightEmUpE.wss", db-5, 1, 200};
		titles[] = {};
	};

	// //////////////////////////////////////
	// Dubbing West Combat
	// //////////////////////////////////////

	class West_Combat_v01
	{
		name = "West_Combat_v01";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\combat\BadGuysE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Combat_v02
	{
		name = "West_Combat_v02";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\combat\CarefulJustBeCareful.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Combat_v03
	{
		name = "West_Combat_v03";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\combat\ContactE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Combat_v04
	{
		name = "West_Combat_v04";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\combat\CoveringFireE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Combat_v05
	{
		name = "West_Combat_v05";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\combat\CoveringGoE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Combat_v06
	{
		name = "West_Combat_v06";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\combat\CoverMeE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Combat_v07
	{
		name = "West_Combat_v07";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\combat\EngagingE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Combat_v08
	{
		name = "West_Combat_v08";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\combat\FocusE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Combat_v09
	{
		name = "West_Combat_v09";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\combat\FuckE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Combat_v10
	{
		name = "West_Combat_v10";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\combat\GetDownE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Combat_v11
	{
		name = "West_Combat_v11";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\combat\GoE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Combat_v12
	{
		name = "West_Combat_v12";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\combat\GotATargetE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Combat_v13
	{
		name = "West_Combat_v13";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\combat\HangOnILLSuppressE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Combat_v14
	{
		name = "West_Combat_v14";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\combat\HitTheDeckE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Combat_v15
	{
		name = "West_Combat_v15";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\combat\HolyShitE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Combat_v16
	{
		name = "West_Combat_v16";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\combat\MotherfuckerE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Combat_v17
	{
		name = "West_Combat_v17";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\combat\MotherfuckerE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Combat_v18
	{
		name = "West_Combat_v18";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\combat\MoveE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Combat_v19
	{
		name = "West_Combat_v19";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\combat\MoveOutE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Combat_v20
	{
		name = "West_Combat_v20";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\combat\OhJesusE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Combat_v21
	{
		name = "West_Combat_v21";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\combat\RightThereE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Combat_v22
	{
		name = "West_Combat_v22";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\combat\ShitE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Combat_v23
	{
		name = "West_Combat_v23";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\combat\SweetJesusE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Combat_v24
	{
		name = "West_Combat_v24";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\combat\TakeCoverE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Combat_v25
	{
		name = "West_Combat_v25";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\combat\TakeHimOutE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Combat_v26
	{
		name = "West_Combat_v26";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\combat\TargetE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Combat_v27
	{
		name = "West_Combat_v27";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\combat\WhatAMess.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Combat_v28
	{
		name = "West_Combat_v28";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\combat\WhatTheFuckQ.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Combat_v29
	{
		name = "West_Combat_v29";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\combat\WhatTheHellQ.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Combat_v30
	{
		name = "West_Combat_v30";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\combat\woohoo1.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Combat_v31
	{
		name = "West_Combat_v31";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\combat\BadGuysE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Combat_v32
	{
		name = "West_Combat_v32";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\combat\CarefulJustBeCareful.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Combat_v33
	{
		name = "West_Combat_v33";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\combat\ContactE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Combat_v34
	{
		name = "West_Combat_v34";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\combat\CoveringFireE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Combat_v35
	{
		name = "West_Combat_v35";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\combat\CoveringGoE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Combat_v36
	{
		name = "West_Combat_v36";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\combat\CoverMeE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Combat_v37
	{
		name = "West_Combat_v37";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\combat\EngagingE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Combat_v38
	{
		name = "West_Combat_v38";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\combat\FocusE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Combat_v39
	{
		name = "West_Combat_v39";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\combat\FuckE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Combat_v40
	{
		name = "West_Combat_v40";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\combat\GetDownE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Combat_v41
	{
		name = "West_Combat_v41";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\combat\GoE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Combat_v42
	{
		name = "West_Combat_v42";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\combat\GotATargetE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Combat_v43
	{
		name = "West_Combat_v43";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\combat\HangOnILLSuppressE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Combat_v44
	{
		name = "West_Combat_v44";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\combat\HitTheDeckE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Combat_v45
	{
		name = "West_Combat_v45";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\combat\HolyShitE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Combat_v46
	{
		name = "West_Combat_v46";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\combat\MotherfuckerE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Combat_v47
	{
		name = "West_Combat_v47";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\combat\MotherfuckerE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Combat_v48
	{
		name = "West_Combat_v48";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\combat\MoveE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Combat_v49
	{
		name = "West_Combat_v49";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\combat\MoveOutE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Combat_v50
	{
		name = "West_Combat_v50";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\combat\OhJesusE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Combat_v51
	{
		name = "West_Combat_v51";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\combat\RightThereE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Combat_v52
	{
		name = "West_Combat_v52";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\combat\ShitE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Combat_v53
	{
		name = "West_Combat_v53";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\combat\SweetJesusE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Combat_v54
	{
		name = "West_Combat_v54";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\combat\TakeCoverE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Combat_v55
	{
		name = "West_Combat_v55";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\combat\TakeHimOutE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Combat_v56
	{
		name = "West_Combat_v56";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\combat\TargetE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Combat_v57
	{
		name = "West_Combat_v57";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\combat\WhatAMess.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Combat_v58
	{
		name = "West_Combat_v58";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\combat\WhatTheFuckQ.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Combat_v59
	{
		name = "West_Combat_v59";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\combat\WhatTheHellQ.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Combat_v60
	{
		name = "West_Combat_v60";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\combat\woohoo1.wss", db-5, 1, 200};
		titles[] = {};
	};

	// //////////////////////////////////////
	// Dubbing East Combat
	// //////////////////////////////////////

	class East_Combat_v01
	{
		name = "East_Combat_v01";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\combat\BadGuysE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Combat_v02
	{
		name = "East_Combat_v02";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\combat\CarefulJustBeCareful.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Combat_v03
	{
		name = "East_Combat_v03";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\combat\ContactE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Combat_v04
	{
		name = "East_Combat_v04";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\combat\CoveringFireE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Combat_v05
	{
		name = "East_Combat_v05";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\combat\CoveringGoE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Combat_v06
	{
		name = "East_Combat_v06";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\combat\CoverMeE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Combat_v07
	{
		name = "East_Combat_v07";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\combat\EngagingE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Combat_v08
	{
		name = "East_Combat_v08";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\combat\FocusE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Combat_v09
	{
		name = "East_Combat_v09";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\combat\GetDownE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Combat_v10
	{
		name = "East_Combat_v10";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\combat\FuckE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Combat_v11
	{
		name = "East_Combat_v11";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\combat\GoE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Combat_v12
	{
		name = "East_Combat_v12";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\combat\GotATargetE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Combat_v13
	{
		name = "East_Combat_v13";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\combat\HangOnILLSuppressE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Combat_v14
	{
		name = "East_Combat_v14";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\combat\HitTheDeckE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Combat_v15
	{
		name = "East_Combat_v15";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\combat\HolyShitE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Combat_v16
	{
		name = "East_Combat_v16";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\combat\MotherfuckerE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Combat_v17
	{
		name = "East_Combat_v17";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\combat\MotherfuckerE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Combat_v18
	{
		name = "East_Combat_v18";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\combat\MoveE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Combat_v19
	{
		name = "East_Combat_v19";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\combat\MoveOutE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Combat_v20
	{
		name = "East_Combat_v20";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\combat\OhJesusE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Combat_v21
	{
		name = "East_Combat_v21";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\combat\RightThereE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Combat_v22
	{
		name = "East_Combat_v22";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\combat\ShitE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Combat_v23
	{
		name = "East_Combat_v23";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\combat\SweetJesusE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Combat_v24
	{
		name = "East_Combat_v24";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\combat\TakeCoverE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Combat_v25
	{
		name = "East_Combat_v25";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\combat\TakeHimOutE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Combat_v26
	{
		name = "East_Combat_v26";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\combat\TargetE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Combat_v27
	{
		name = "East_Combat_v27";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\combat\WhatAMess.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Combat_v28
	{
		name = "East_Combat_v28";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\combat\WhatTheFuckQ.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Combat_v29
	{
		name = "East_Combat_v29";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\combat\WhatTheHellQ.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Combat_v30
	{
		name = "East_Combat_v30";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\combat\woohoo1.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Combat_v31
	{
		name = "East_Combat_v31";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\combat\BadGuysE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Combat_v32
	{
		name = "East_Combat_v32";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\combat\CarefulJustBeCareful.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Combat_v33
	{
		name = "East_Combat_v33";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\combat\ContactE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Combat_v34
	{
		name = "East_Combat_v34";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\combat\CoveringFireE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Combat_v35
	{
		name = "East_Combat_v35";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\combat\CoveringGoE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Combat_v36
	{
		name = "East_Combat_v36";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\combat\CoverMeE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Combat_v37
	{
		name = "East_Combat_v37";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\combat\EngagingE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Combat_v38
	{
		name = "East_Combat_v38";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\combat\FocusE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Combat_v39
	{
		name = "East_Combat_v39";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\combat\GetDownE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Combat_v40
	{
		name = "East_Combat_v40";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\combat\FuckE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Combat_v41
	{
		name = "East_Combat_v41";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\combat\GoE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Combat_v42
	{
		name = "East_Combat_v42";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\combat\GotATargetE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Combat_v43
	{
		name = "East_Combat_v43";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\combat\HangOnILLSuppressE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Combat_v44
	{
		name = "East_Combat_v44";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\combat\HitTheDeckE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Combat_v45
	{
		name = "East_Combat_v45";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\combat\HolyShitE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Combat_v46
	{
		name = "East_Combat_v46";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\combat\MotherfuckerE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Combat_v47
	{
		name = "East_Combat_v47";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\combat\MotherfuckerE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Combat_v48
	{
		name = "East_Combat_v48";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\combat\MoveE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Combat_v49
	{
		name = "East_Combat_v49";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\combat\MoveOutE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Combat_v50
	{
		name = "East_Combat_v50";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\combat\OhJesusE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Combat_v51
	{
		name = "East_Combat_v51";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\combat\RightThereE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Combat_v52
	{
		name = "East_Combat_v52";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\combat\ShitE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Combat_v53
	{
		name = "East_Combat_v53";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\combat\SweetJesusE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Combat_v54
	{
		name = "East_Combat_v54";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\combat\TakeCoverE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Combat_v55
	{
		name = "East_Combat_v55";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\combat\TakeHimOutE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Combat_v56
	{
		name = "East_Combat_v56";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\combat\TargetE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Combat_v57
	{
		name = "East_Combat_v57";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\combat\WhatAMess.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Combat_v58
	{
		name = "East_Combat_v58";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\combat\WhatTheFuckQ.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Combat_v59
	{
		name = "East_Combat_v59";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\combat\WhatTheHellQ.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Combat_v60
	{
		name = "East_Combat_v60";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\combat\woohoo1.wss", db-5, 1, 200};
		titles[] = {};
	};

	// //////////////////////////////////////
	// Dubbing West Advancing
	// //////////////////////////////////////

	class West_Advancing_v01
	{
		name = "West_Advancing_v01";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\combat\WhatTheFuckQ.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Advancing_v02
	{
		name = "West_Advancing_v02";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\combat\WhatTheFuckWasThatQ.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Advancing_v03
	{
		name = "West_Advancing_v03";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\combat\WhatTheHellQ.wss", db-5, 1, 200};
		titles[] = {};
	};

	// //////////////////////////////////////
	// Dubbing East Advancing
	// //////////////////////////////////////

	class East_Advancing_v01
	{
		name = "East_Advancing_v01";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\combat\WhatTheFuckQ.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Advancing_v02
	{
		name = "East_Advancing_v02";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\combat\WhatTheFuckWasThatQ.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Advancing_v03
	{
		name = "East_Advancing_v03";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\combat\WhatTheHellQ.wss", db-5, 1, 200};
		titles[] = {};
	};

	// //////////////////////////////////////
	// Dubbing West Suppressed
	// //////////////////////////////////////

	class West_Suppressed_v01
	{
		name = "West_Suppressed_v01";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\combat\CoveringFireE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Suppressed_v02
	{
		name = "West_Suppressed_v02";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\combat\CoveringGoE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Suppressed_v03
	{
		name = "West_Suppressed_v03";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\combat\CoverMeE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Suppressed_v04
	{
		name = "West_Suppressed_v04";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\combat\GetDownE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Suppressed_v05
	{
		name = "West_Suppressed_v05";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\combat\HangOnILLSuppressE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Suppressed_v06
	{
		name = "West_Suppressed_v06";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\combat\HitTheDeckE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Suppressed_v07
	{
		name = "West_Suppressed_v07";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\combat\SuppresiveFireE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Suppressed_v08
	{
		name = "West_Suppressed_v08";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\combat\SuppressingE.wss", db-5, 1, 200};
		titles[] = {};
	};

	// //////////////////////////////////////
	// Dubbing East Suppressed
	// //////////////////////////////////////

	class East_Suppressed_v01
	{
		name = "East_Suppressed_v01";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\combat\CoveringFireE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Suppressed_v02
	{
		name = "East_Suppressed_v02";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\combat\CoveringGoE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Suppressed_v03
	{
		name = "East_Suppressed_v03";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\combat\CoverMeE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Suppressed_v04
	{
		name = "East_Suppressed_v04";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\combat\GetDownE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Suppressed_v05
	{
		name = "East_Suppressed_v05";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\combat\HangOnILLSuppressE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Suppressed_v06
	{
		name = "East_Suppressed_v06";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\combat\HitTheDeckE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Suppressed_v07
	{
		name = "East_Suppressed_v07";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\combat\SuppresiveFireE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Suppressed_v08
	{
		name = "East_Suppressed_v08";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\combat\SuppressingE.wss", db-5, 1, 200};
		titles[] = {};
	};

	// //////////////////////////////////////
	// Dubbing West Reloading
	// //////////////////////////////////////

	class West_Reloading_v01
	{
		name = "West_Reloading_v01";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\combat\ChangingMagsE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Reloading_v02
	{
		name = "West_Reloading_v02";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\combat\CoverMeWhileIReloadE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Reloading_v03
	{
		name = "West_Reloading_v03";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\combat\IgottaReloadE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Reloading_v04
	{
		name = "West_Reloading_v04";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\combat\ReloadingE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Reloading_v05
	{
		name = "West_Reloading_v05";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\combat\ChangingMagsE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Reloading_v06
	{
		name = "West_Reloading_v06";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\combat\CoverMeWhileIReloadE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Reloading_v07
	{
		name = "West_Reloading_v07";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\combat\IgottaReloadE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Reloading_v08
	{
		name = "West_Reloading_v08";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\combat\ReloadingE.wss", db-5, 1, 200};
		titles[] = {};
	};

	// //////////////////////////////////////
	// Dubbing East Reloading
	// //////////////////////////////////////

	class East_Reloading_v01
	{
		name = "East_Reloading_v01";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\combat\ChangingMagsE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Reloading_v02
	{
		name = "East_Reloading_v02";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\combat\CoverMeWhileIReloadE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Reloading_v03
	{
		name = "East_Reloading_v03";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\combat\IgottaReloadE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Reloading_v04
	{
		name = "East_Reloading_v04";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\combat\ReloadingE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Reloading_v05
	{
		name = "East_Reloading_v05";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\combat\ChangingMagsE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Reloading_v06
	{
		name = "East_Reloading_v06";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\combat\CoverMeWhileIReloadE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Reloading_v07
	{
		name = "East_Reloading_v07";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\combat\IgottaReloadE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Reloading_v08
	{
		name = "East_Reloading_v08";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\combat\ReloadingE.wss", db-5, 1, 200};
		titles[] = {};
	};

	// //////////////////////////////////////
	// Dubbing West Grenade
	// //////////////////////////////////////

	class West_Grenade_v01
	{
		name = "West_Grenade_v01";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\combat\GrenadeE.wss", db+10, 1, 200};
		titles[] = {};
	};

	class West_Grenade_v02
	{
		name = "West_Grenade_v02";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\combat\WatchForGrenadeE.wss", db+10, 1, 200};
		titles[] = {};
	};

	class West_Grenade_v03
	{
		name = "West_Grenade_v03";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\combat\GrenadeE.wss", db+10, 1, 200};
		titles[] = {};
	};

	class West_Grenade_v04
	{
		name = "West_Grenade_v04";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\combat\WatchForGrenadeE.wss", db+10, 1, 200};
		titles[] = {};
	};

	// //////////////////////////////////////
	// Dubbing East Grenade
	// //////////////////////////////////////

	class East_Grenade_v01
	{
		name = "East_Grenade_v01";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\combat\GrenadeE.wss", db+10, 1, 200};
		titles[] = {};
	};

	class East_Grenade_v02
	{
		name = "East_Grenade_v02";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\combat\WatchForGrenadeE.wss", db+10, 1, 200};
		titles[] = {};
	};

	class East_Grenade_v03
	{
		name = "East_Grenade_v03";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\combat\GrenadeE.wss", db+10, 1, 200};
		titles[] = {};
	};

	class East_Grenade_v04
	{
		name = "East_Grenade_v04";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\combat\WatchForGrenadeE.wss", db+10, 1, 200};
		titles[] = {};
	};

	// //////////////////////////////////////
	// Dubbing West Incoming
	// //////////////////////////////////////

	class West_Incoming_v01
	{
		name = "West_Incoming_v01";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\GetDown.wss", db+10, 1, 200};
		titles[] = {};
	};

	class West_Incoming_v02
	{
		name = "West_Incoming_v02";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\IncomingE.wss", db+10, 1, 200};
		titles[] = {};
	};

	class West_Incoming_v03
	{
		name = "West_Incoming_v03";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\TakeCoverE.wss", db+10, 1, 200};
		titles[] = {};
	};

	// //////////////////////////////////////
	// Dubbing East Incoming
	// //////////////////////////////////////

	class East_Incoming_v01
	{
		name = "East_Incoming_v01";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\GetDown.wss", db+10, 1, 200};
		titles[] = {};
	};

	class East_Incoming_v02
	{
		name = "East_Incoming_v02";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\IncomingE.wss", db+10, 1, 200};
		titles[] = {};
	};

	class East_Incoming_v03
	{
		name = "East_Incoming_v03";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\TakeCoverE.wss", db+10, 1, 200};
		titles[] = {};
	};

	// //////////////////////////////////////
	// Dubbing East Scream
	// //////////////////////////////////////

	class East_Scream_v01
	{
		name = "East_Scream_v01";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\ALICE\Screams\Scream_01.wss", db+10, 1, 200};
		titles[] = {};
	};

	class East_Scream_v02
	{
		name = "East_Scream_v02";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\ALICE\Screams\Scream_02.wss", db+10, 1, 200};
		titles[] = {};
	};

	class East_Scream_v03
	{
		name = "East_Scream_v03";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\ALICE\Screams\Scream_03.wss", db+10, 1, 200};
		titles[] = {};
	};

	class East_Scream_v04
	{
		name = "East_Scream_v04";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\ALICE\Screams\Scream_04.wss", db+10, 1, 200};
		titles[] = {};
	};

	class East_Scream_v05
	{
		name = "East_Scream_v05";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\ALICE\Screams\Scream_05.wss", db+10, 1, 200};
		titles[] = {};
	};

	class East_Scream_v06
	{
		name = "East_Scream_v06";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\ALICE\Screams\Scream_06.wss", db+10, 1, 200};
		titles[] = {};
	};

	class East_Scream_v07
	{
		name = "East_Scream_v07";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\ALICE\Screams\Scream_07.wss", db+10, 1, 200};
		titles[] = {};
	};

	class East_Scream_v08
	{
		name = "East_Scream_v08";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\ALICE\Screams\Scream_08.wss", db+10, 1, 200};
		titles[] = {};
	};

	class East_Scream_v09
	{
		name = "East_Scream_v09";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\ALICE\Screams\Scream_01.wss", db+10, 1, 200};
		titles[] = {};
	};

	class East_Scream_v10
	{
		name = "East_Scream_v10";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\ALICE\Screams\Scream_02.wss", db+10, 1, 200};
		titles[] = {};
	};

	class East_Scream_v11
	{
		name = "East_Scream_v11";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\ALICE\Screams\Scream_03.wss", db+10, 1, 200};
		titles[] = {};
	};

	class East_Scream_v12
	{
		name = "East_Scream_v12";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\ALICE\Screams\Scream_04.wss", db+10, 1, 200};
		titles[] = {};
	};

	class East_Scream_v13
	{
		name = "East_Scream_v13";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\ALICE\Screams\Scream_05.wss", db+10, 1, 200};
		titles[] = {};
	};

	class East_Scream_v14
	{
		name = "East_Scream_v14";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\ALICE\Screams\Scream_06.wss", db+10, 1, 200};
		titles[] = {};
	};

	class East_Scream_v15
	{
		name = "East_Scream_v15";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\ALICE\Screams\Scream_07.wss", db+10, 1, 200};
		titles[] = {};
	};

	class East_Scream_v16
	{
		name = "East_Scream_v16";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\ALICE\Screams\Scream_08.wss", db+10, 1, 200};
		titles[] = {};
	};

	// //////////////////////////////////////
	// Dubbing West Woohoo
	// //////////////////////////////////////

	class West_Woohoo_v01
	{
		name = "West_Woohoo_v01";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\combat\woohoo1.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Woohoo_v02
	{
		name = "West_Woohoo_v02";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\combat\woohoo2.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Woohoo_v03
	{
		name = "West_Woohoo_v03";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\combat\woohoo3.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Woohoo_v04
	{
		name = "West_Woohoo_v04";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\combat\woohoo4.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Woohoo_v05
	{
		name = "West_Woohoo_v05";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\combat\woohoo1.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Woohoo_v06
	{
		name = "West_Woohoo_v06";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\combat\woohoo2.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Woohoo_v07
	{
		name = "West_Woohoo_v07";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\combat\woohoo3.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Woohoo_v08
	{
		name = "West_Woohoo_v08";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\EN\combat\woohoo4.wss", db-5, 1, 200};
		titles[] = {};
	};

	// //////////////////////////////////////
	// Dubbing East Woohoo
	// //////////////////////////////////////

	class East_Woohoo_v01
	{
		name = "East_Woohoo_v01";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\combat\woohoo1.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Woohoo_v02
	{
		name = "East_Woohoo_v02";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\combat\woohoo2.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Woohoo_v03
	{
		name = "East_Woohoo_v03";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\combat\woohoo3.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Woohoo_v04
	{
		name = "East_Woohoo_v04";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\combat\woohoo4.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Woohoo_v05
	{
		name = "East_Woohoo_v05";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\combat\woohoo1.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Woohoo_v06
	{
		name = "East_Woohoo_v06";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\combat\woohoo2.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Woohoo_v07
	{
		name = "East_Woohoo_v07";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\combat\woohoo3.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Woohoo_v08
	{
		name = "East_Woohoo_v08";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male02\RU\combat\woohoo4.wss", db-5, 1, 200};
		titles[] = {};
	};

	// //////////////////////////////////////
	// Dubbing West Move
	// //////////////////////////////////////

	class West_Move_v01
	{
		name = "West_Move_v01";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\FallBackE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Move_v02
	{
		name = "West_Move_v02";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\FollowMyLeadE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Move_v03
	{
		name = "West_Move_v03";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\FormOnMeE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class West_Move_v04
	{
		name = "West_Move_v04";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\EN\GoGoGoE.wss", db-5, 1, 200};
		titles[] = {};
	};

	// //////////////////////////////////////
	// Dubbing East Move
	// //////////////////////////////////////

	class East_Move_v01
	{
		name = "East_Move_v01";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\FallBackE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Move_v02
	{
		name = "East_Move_v02";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\FollowMyLeadE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Move_v03
	{
		name = "East_Move_v03";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\FormOnMeE.wss", db-5, 1, 200};
		titles[] = {};
	};

	class East_Move_v04
	{
		name = "East_Move_v04";
		sound[] = {"\ca\dubbing\GLOBAL\RADIO\Male01\RU\GoGoGoE.wss", db-5, 1, 200};
		titles[] = {};
	};
};