// ////////////////////////////////////////////////////////////////////////////
// GL5 v.1.0 Dubbing Config
// 
// ////////////////////////////////////////////////////////////////////////////

class CfgPatches 
{
	class GL5_Dubbing
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 1.0;
		requiredAddons[] = {};
	};
};

#include "CfgSounds.hpp"