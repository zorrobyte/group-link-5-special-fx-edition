// ////////////////////////////////////////////////////////////////////////////
// GL5 v.1.0 Shell FX Config
//
// ////////////////////////////////////////////////////////////////////////////

class CfgPatches
{
	class GL5_Shell_FX
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 1.04;
		requiredAddons[] = {"A3_Data_F", "A3_Weapons_F"};
	};
};

#include "CfgAmmo.hpp"

class CfgVehicles
{
	class NonStrategic;

	class GL5_Crater : NonStrategic
	{
		scope = 2;

		model = "\A3\data_f\krater.p3d";
	};
};

class CfgAddOns
{
	class PreloadBanks {};
	
	class PreloadAddOns
	{
		class GL5_Shell_FX
		{
			list[] = {"GL5_Shell_FX"};
		};
	};
};